<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Plugin;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface PluginMapInterface {

}
