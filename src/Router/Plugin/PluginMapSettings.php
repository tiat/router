<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Plugin;

//
use Tiat\Mvc\Controller\ControllerInterface;
use Tiat\Router\Router\Type\Plugin\RouterPluginType;
use Tiat\Stdlib\Parameters\ParametersPlugin;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class PluginMapSettings extends AbstractPluginMap implements PluginMapSettingsInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @var string
	 * @since 3.0.0 First time introduced.
	 */
	private string $_route;
	
	/**
	 * @var RouterPluginType
	 * @since 3.0.0 First time introduced.
	 */
	private RouterPluginType $_type;
	
	/**
	 * @var ControllerInterface|string
	 * @since 3.0.0 First time introduced.
	 */
	private ControllerInterface|string $_controller;
	
	/**
	 * @var string
	 * @since 3.0.0 First time introduced.
	 */
	private string $_action;
	
	/**
	 * @var PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	private PluginMapSettingsInterface $_child;
	
	/**
	 * @param    null|PluginMapSettingsInterface|iterable    $settings
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(PluginMapSettingsInterface|iterable $settings = NULL) {
	
	}
	
	/**
	 * @param    array|string    $verbs
	 *
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function addVerbs(array|string $verbs) : static {
		return $this;
	}
	
	/**
	 * @param    string    $verb
	 *
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function removeVerbs(string $verb) : static {
		return $this;
	}
	
	/**
	 * @param    string    $route
	 *
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function addRoute(string $route) : static {
		//
		$this->_route = $route;
		
		return $this;
	}
	
	/**
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function removeRoute() : static {
		return $this;
	}
	
	/**
	 * @param    RouterPluginType    $type
	 *
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function addType(RouterPluginType $type) : static {
		//
		$this->_type = $type;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function removeType() : static {
		return $this;
	}
	
	/**
	 * @param    ControllerInterface|string    $controller
	 *
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function addController(ControllerInterface|string $controller) : static {
		//
		$this->_controller = $controller;
		
		return $this;
	}
	
	/**
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function removeController() : static {
		return $this;
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function addAction(string $action) : static {
		//
		$this->_action = $action;
		
		return $this;
	}
	
	/**
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function removeAction() : static {
		return $this;
	}
	
	/**
	 * @param    PluginMapSettingsInterface    $settings
	 *
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function addChild(PluginMapSettingsInterface $settings) : static {
		//
		$this->_child = $settings;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since 3.0.0 First time introduced.
	 */
	public function removeChild() : static {
		return $this;
	}
}
