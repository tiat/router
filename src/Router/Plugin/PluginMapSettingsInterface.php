<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Plugin;

//
use Tiat\Mvc\Controller\ControllerInterface;
use Tiat\Router\Router\Type\Plugin\RouterPluginType;
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface PluginMapSettingsInterface extends ParametersPluginInterface {
	
	/**
	 * @param    array|string    $verbs
	 *
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function addVerbs(array|string $verbs) : PluginMapSettingsInterface;
	
	/**
	 * @param    string    $verb
	 *
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function removeVerbs(string $verb) : PluginMapSettingsInterface;
	
	/**
	 * @param    string    $route
	 *
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function addRoute(string $route) : PluginMapSettingsInterface;
	
	/**
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function removeRoute() : PluginMapSettingsInterface;
	
	/**
	 * @param    RouterPluginType    $type
	 *
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function addType(RouterPluginType $type) : PluginMapSettingsInterface;
	
	/**
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function removeType() : PluginMapSettingsInterface;
	
	/**
	 * @param    ControllerInterface|string    $controller
	 *
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function addController(ControllerInterface|string $controller) : PluginMapSettingsInterface;
	
	/**
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function removeController() : PluginMapSettingsInterface;
	
	/**
	 * @param    string    $action
	 *
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function addAction(string $action) : PluginMapSettingsInterface;
	
	/**
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function removeAction() : PluginMapSettingsInterface;
	
	/**
	 * @param    PluginMapSettingsInterface    $settings
	 *
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function addChild(PluginMapSettingsInterface $settings) : PluginMapSettingsInterface;
	
	/**
	 * @return PluginMapSettingsInterface
	 * @since 3.0.0 First time introduced.
	 */
	public function removeChild() : PluginMapSettingsInterface;
}
