<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router;

//
use Tiat\Router\Router\Plugin\PluginMapInterface;
use Tiat\Router\Router\Plugin\PluginMapSettingsInterface;
use Tiat\Router\Router\Type\Plugin\PluginRouterParamsInterface;
use Tiat\Standard\DataModel\HttpMethod;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Addition of PluginMapSettingsInterface && Get the original resolved route.
 * @since   3.1.0 Addition of addVariables()
 * @since   3.1.0 Addition of getVariables()
 * @since   3.1.0 Addition of addConstraints()
 * @since   3.1.0 Addition of getConstraints()
 * @since   3.1.1 Addition of addRouteMethod()
 */
interface MapInterface {
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getQuery() : string;
	
	/**
	 * @param    array    $query
	 *
	 * @return AbstractMap
	 * @since   3.0.0 First time introduced.
	 */
	public function setQuery(array $query) : MapInterface;
	
	/**
	 * Return resolved/mapped route as array.
	 * Array has $key=>$value format where the $key is RouterPart::case->value
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRoute() : ?array;
	
	/**
	 * Set route for mapping.
	 *
	 * @param    array    $route
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setRoute(array $route) : void;
	
	/**
	 * Get the original resolved route as array.
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRouteResolved() : ?array;
	
	/**
	 * @param    array|string    $route
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function explodeRoute(array|string $route) : array;
	
	/**
	 * Get the route tree which is followed.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultRouteTree() : array;
	
	/**
	 * Reset default route tree back to default.
	 *
	 * @return AbstractMap
	 * @since   3.0.0 First time introduced.
	 */
	public function resetDefaultRouteTree() : MapInterface;
	
	/**
	 * Update the default route tree with custom parameters. This is usually triggered from setDefaultPart{part} methods.
	 *
	 * @return AbstractMap
	 * @since   3.0.0 First time introduced.
	 */
	public function updateDefaultRouteTree() : MapInterface;
	
	/**
	 * Return the whole route as formatted (and pre-checked).
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function toString() : string;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function __toString() : string;
	
	/**
	 * Return resolved route in array (and pre-checked).
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function toArray() : array;
	
	/**
	 * Resolve full namespace name from resolved route map or with default values. Default format: Application\Default\Controller
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getNamespaceName() : ?string;
	
	/**
	 * Return module name from resolved route map.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getModuleName() : ?string;
	
	/**
	 * Resolve controller name from resolved route map. Default format: IndexController
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerName() : ?string;
	
	/**
	 * Resolve action name from resolved route map.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getActionName() : ?string;
	
	/**
	 * Update module & controller name from full controller name.
	 * Notice! The namespace is not possible to replace directly so it will be ignored.
	 *
	 * @param    string    $fullControllerName
	 *
	 * @return MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function replaceModuleWithControllerName(string $fullControllerName) : MapInterface;
	
	/**
	 * @param    string    $module
	 *
	 * @return MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function replaceModuleName(string $module) : MapInterface;
	
	/**
	 * @param    string    $controller
	 *
	 * @return MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function replaceControllerName(string $controller) : MapInterface;
	
	/**
	 * Replace action name in the route.
	 * This method allows you to replace the action name in the route. Use with carefully to avoid unintended consequences.
	 * This is usually used with HTTP METHOD based routing & with custom routes.
	 *
	 * @param    string    $action
	 *
	 * @return MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function replaceActionName(string $action) : MapInterface;
	
	/**
	 * If language has been set in query then save it.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getLanguage() : ?array;
	
	/**
	 * @param    array|string    $language
	 *
	 * @return MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setLanguage(array|string $language) : MapInterface;
	
	/**
	 * @return null|PluginMapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettings() : ?PluginMapSettingsInterface;
	
	/**
	 * @param    array    $route
	 *
	 * @return MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettings(array $route) : MapInterface;
	
	/**
	 * Populate the route map with the given route and tree arrays.
	 *
	 * @param    array    $route    The route array containing route information.
	 * @param    array    $tree     The tree array containing tree information.
	 *
	 * @return array The populated route map array.
	 * @since   3.0.0 First time introduced.
	 */
	public function populateMap(array $route, array $tree) : array;
	
	/**
	 * Resolve the base name of the controller from resolved route map or with default values.
	 * Default format: Controller
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerBaseName() : ?string;
	
	/**
	 * Set the error controller for the application.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function setErrorController() : void;
	
	/**
	 * @param    array    $current
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouteCurrent(array $current) : void;
	
	/**
	 * Retrieve the error controller from the configuration or return null if not found.
	 *
	 * @return null|string The error controller class name if found, otherwise null.
	 * @since   3.0.0 First time introduced.
	 */
	public function getErrorController() : ?string;
	
	/**
	 * Get the name of the error action.
	 *
	 * @return null|string The name of the error action.
	 * @since   3.0.0 First time introduced.
	 */
	public function getErrorAction() : ?string;
	
	/**
	 * @param    PluginRouterParamsInterface    $params
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function addVariables(PluginRouterParamsInterface $params) : MapInterface;
	
	/**
	 * @return null|PluginRouterParamsInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getVariables() : ?PluginRouterParamsInterface;
	
	/**
	 * @param    PluginRouterParamsInterface    $constraints
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function addConstraints(PluginRouterParamsInterface $constraints) : MapInterface;
	
	/**
	 * @return null|PluginRouterParamsInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getConstraints() : ?PluginRouterParamsInterface;
	
	/**
	 * @return null|HttpMethod
	 * @since   3.1.1 First time introduced.
	 */
	public function getRouteMethod() : ?HttpMethod;
	
	/**
	 * @param    HttpMethod    $method
	 *
	 * @return MapInterface
	 * @since   3.1.1 First time introduced.
	 */
	public function setRouteMethod(HttpMethod $method) : MapInterface;
}
