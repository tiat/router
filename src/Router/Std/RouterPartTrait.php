<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Std;

//
use Tiat\Router\Std\RouterPart;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait RouterPartTrait {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterPart DEFAULT_NAMESPACE_PART = RouterPart::NAMESPACE;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterPart DEFAULT_MODULE_PART = RouterPart::MODULE;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterPart DEFAULT_CONTROLLER_PART = RouterPart::CONTROLLER;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterPart DEFAULT_ACTION_PART = RouterPart::ACTION;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterPart DEFAULT_FALLBACK_PART = RouterPart::FALLBACK;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterPart DEFAULT_ERROR_PART = RouterPart::ERROR;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterPart DEFAULT_CUSTOM_PART = RouterPart::CUSTOM;
	
	/**
	 * @var RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	private RouterPart $_defaultPartNamespace = self::DEFAULT_NAMESPACE_PART;
	
	/**
	 * @var RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	private RouterPart $_defaultPartModule = self::DEFAULT_MODULE_PART;
	
	/**
	 * @var RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	private RouterPart $_defaultPartController = self::DEFAULT_CONTROLLER_PART;
	
	/**
	 * @var RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	private RouterPart $_defaultPartAction = self::DEFAULT_ACTION_PART;
	
	/**
	 * @var RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	private RouterPart $_defaultPartFallback = self::DEFAULT_FALLBACK_PART;
	
	/**
	 * @var RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	private RouterPart $_defaultPartError = self::DEFAULT_ERROR_PART;
	
	/**
	 * @var RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	private RouterPart $_defaultPartCustom = self::DEFAULT_CUSTOM_PART;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartNamespace() : RouterPart {
		return $this->_defaultPartNamespace;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartNamespace(RouterPart $part) : RouterPartInterface {
		//
		$this->_defaultPartNamespace = $part;
		
		//
		return $this;
	}
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartModule() : RouterPart {
		return $this->_defaultPartModule;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartModule(RouterPart $part) : RouterPartInterface {
		//
		$this->_defaultPartModule = $part;
		
		//
		return $this;
	}
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartController() : RouterPart {
		return $this->_defaultPartController;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartController(RouterPart $part) : RouterPartInterface {
		//
		$this->_defaultPartController = $part;
		
		//
		return $this;
	}
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartView() : RouterPart {
		return $this->getDefaultPartAction();
	}
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartAction() : RouterPart {
		return $this->_defaultPartAction;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartAction(RouterPart $part) : RouterPartInterface {
		//
		$this->_defaultPartAction = $part;
		
		//
		return $this;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartView(RouterPart $part) : RouterPartInterface {
		return $this->setDefaultPartAction($part);
	}
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartFallback() : RouterPart {
		return $this->_defaultPartFallback;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartFallback(RouterPart $part) : RouterPartInterface {
		//
		$this->_defaultPartFallback = $part;
		
		//
		return $this;
	}
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartError() : RouterPart {
		return $this->_defaultPartError;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartError(RouterPart $part) : RouterPartInterface {
		//
		$this->_defaultPartError = $part;
		
		//
		return $this;
	}
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartDefault() : RouterPart {
		return $this->getDefaultPartCustom();
	}
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartCustom() : RouterPart {
		return $this->_defaultPartCustom;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartCustom(RouterPart $part) : RouterPartInterface {
		//
		$this->_defaultPartCustom = $part;
		
		//
		return $this;
	}
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartDefault(RouterPart $part) : RouterPartInterface {
		return $this->setDefaultPartCustom($part);
	}
}
