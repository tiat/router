<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Std;

//
use Tiat\Router\Std\RouterPart;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RouterPartInterface {
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartNamespace() : RouterPart;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartNamespace(RouterPart $part) : RouterPartInterface;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartModule() : RouterPart;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartModule(RouterPart $part) : RouterPartInterface;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartController() : RouterPart;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartController(RouterPart $part) : RouterPartInterface;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartView() : RouterPart;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartAction() : RouterPart;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartAction(RouterPart $part) : RouterPartInterface;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartView(RouterPart $part) : RouterPartInterface;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartFallback() : RouterPart;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartFallback(RouterPart $part) : RouterPartInterface;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartError() : RouterPart;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartError(RouterPart $part) : RouterPartInterface;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartDefault() : RouterPart;
	
	/**
	 * @return RouterPart
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultPartCustom() : RouterPart;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartCustom(RouterPart $part) : RouterPartInterface;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterPartInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDefaultPartDefault(RouterPart $part) : RouterPartInterface;
}
