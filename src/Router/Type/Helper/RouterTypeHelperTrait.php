<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Helper;

//
use Tiat\Config\Config;
use Tiat\Router\Exception\InvalidArgumentException;
use Tiat\Router\Exception\RuntimeException;
use Tiat\Router\Router\MapInterface;
use Tiat\Router\Router\RouteParams;
use Tiat\Router\Router\Type\Plugin\PluginRouterParams;
use Tiat\Router\Router\Type\Plugin\PluginRouterParamsInterface;

use function explode;
use function filter_var;
use function get_class;
use function implode;
use function in_array;
use function mb_convert_encoding;
use function mb_detect_encoding;
use function mb_detect_order;
use function mb_strtolower;
use function method_exists;
use function sprintf;
use function str_starts_with;
use function strlen;
use function strncmp;
use function substr;
use function trigger_error;
use function trim;

/**
 * Class Router
 *  This class is responsible for handling route matching within a predefined route tree, supporting dynamic parameters,
 *  optional segments, and consistent handling of trailing slashes.
 *  ### Key Features:
 *  1. **Dynamic Parameters**: Supports dynamic segments like `:cid` or `:sid` in the routes.
 *  2. **Optional Parameters**: Allows the last dynamic parameter in a route to be optional (e.g., `/service/customer/:cid/api/:sid`).
 *  3. **Optional Segments**: Handles optional segments defined with square brackets, such as `[:edit]`.
 *  4. **Trailing Slash Handling**: Adds a trailing slash to the request for consistent route matching.
 *  5. **Flexible Route Matching**: Converts route keys to regular expressions, allowing flexible matching of routes, even with optional or missing parameters.
 *  6. **Controller Handling**: After a route is matched, missing parameters or extra segments are handled by the controller.
 *  ### Example Usage:
 *  1. **Define Route Keys**: The route tree defines the routes with dynamic parameters and optional segments.
 *     Example:
 *     ```
 *     $this->_routeTree = [
 *         '/service/customer/:cid/api/:sid' => 'MyCustomController::Method',
 *         '/service/customer/:cid/edit' => 'MyAnotherController::Method',
 *         '/service/admin/:cid' => 'MyAdminController::Method',
 *     ];
 *     ```
 *  2. **Request Matching**: Requests such as `/service/customer/555/api/34` or `/service/customer/555/api` are matched against the defined routes.
 *     If the last parameter (e.g., `:sid`) is missing, the controller will handle it.
 *  3. **Trailing Slashes**: The request is automatically adjusted to include a trailing slash if missing to ensure proper matching.
 *  4. **Controller Handling**: The matched route, along with any dynamic parameters, is passed to the controller, which handles any missing or extra parameters.
 *  ### How it Works:
 *  1. **Route Tree**: The class works with a predefined `_routeTree` that contains the route definitions.
 *  2. **Regex Conversion**: Each route key is converted into a regex pattern, with dynamic segments (like `:cid`) translated into capturing groups.
 *  3. **Optional Parameter Handling**: The last dynamic parameter in a route is allowed to be optional.
 *  4. **Route Matching**: The request is checked against each route's regex. If a match is found, the route and any captured parameters are forwarded to the controller.
 *  5. **Handling No Match**: If no match is found, the class returns `FALSE`, indicating that the request does not match any defined routes.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Added support for route placeholders.
 * @since   3.1.0 Added support for route wildcards.
 */
trait RouterTypeHelperTrait {
	
	/**
	 * @var bool
	 * @since   3.1.0 First time introduced.
	 */
	public const bool CHECK_ROUTES = TRUE;
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	public const string DEFAULT_ENCODING = 'UTF-8';
	
	/**
	 * @var MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected MapInterface $_routeDefault;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_routeChild;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_defaultEncoding = self::DEFAULT_ENCODING;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_routes;
	
	/**
	 * Register of all routes
	 *
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_routeTree;
	
	/**
	 * Check if routes should be checked during matching
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_checkRoutes = self::CHECK_ROUTES;
	
	/**
	 * Map that the route can be matched against
	 *
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_routeMap = [RouteParams::ROUTER_PARAMS_KEY->value => NULL,
	                            RouteParams::ROUTER_PARAMS_MATCHES->value => NULL,
	                            RouteParams::ROUTER_PARAMS_OPTIONS->value => NULL];
	
	/**
	 * @param    bool    $status
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setCheckRoutes(bool $status) : RouterTypeHelperInterface {
		//
		$this->_checkRoutes = $status;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 * @param    array     $params
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addRoute(string $name, array $params) : RouterTypeHelperInterface {
		// Create new route plugin with given parameters
		if(isset($this->_routes[$name])):
			$msg = sprintf("Route '%s' already exists", $name);
			throw new RuntimeException($msg);
		else:
			$this->_routes[$name] = ( $plugin = new PluginRouterParams($name, $params) );
		endif;
		
		// Register the route in the tree
		return $this->registerRouteTree($plugin);
	}
	
	/**
	 * @param    PluginRouterParamsInterface    $params
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function registerRouteTree(PluginRouterParamsInterface $params) : RouterTypeHelperInterface {
		// Explode the route into segments
		$options = $params->getParam('options');
		$route   = $options['route'];
		
		//
		$this->_registerRoute($route, $params->getParams(), TRUE);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $route
	 * @param    array     $options
	 * @param    bool      $override
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected function _registerRoute(string $route, array $options = [], bool $override = FALSE) : RouterTypeHelperInterface {
		//
		if(empty($this->_routeTree) || $override === TRUE):
			$this->_routeTree[$route] = $options;
		else:
			$msg = sprintf("Route '%s' already exists in the route tree", $route);
			throw new RuntimeException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $route
	 *
	 * @return null|PluginRouterParams
	 * @since   3.0.0 First time introduced.
	 */
	public function searchRoute(string $route) : ?PluginRouterParams {
		// Check if routes should be checked during matching
		if($this->isCheckRoutesEnabled() === TRUE):
			$this->_checkRoutes();
		endif;
		
		// Search for the route in the route tree
		if(! empty($this->_routes) && $this->_searchRouteTree($route) !== NULL):
			// Get the route options which is PluginRouterParams object
			$r = $this->getRouteMap(RouteParams::ROUTER_PARAMS_OPTIONS->value);
			
			// Get the matched route parameters & add it to PluginRouterParams object
			$r->setParam(RouteParams::ROUTER_PARAMS_MATCHES->value,
			             $this->getRouteMap(RouteParams::ROUTER_PARAMS_MATCHES->value));
			
			//
			return $r;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function isCheckRoutesEnabled() : bool {
		return $this->_checkRoutes;
	}
	
	/**
	 * If routeTree is empty, register routes if exists
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkRoutes() : static {
		if(empty($this->_routeTree) && ( $routes = $this->getRoutes() ) !== NULL):
			$this->registerRoutes($routes);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRoutes() : ?array {
		return $this->_routes ?? NULL;
	}
	
	/**
	 * @param    array    $routes
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Added the ability to register routes from an array.
	 */
	public function registerRoutes(array $routes) : RouterTypeHelperInterface {
		if(! empty($routes)):
			foreach($routes as $route):
				if($route instanceof PluginRouterParamsInterface):
					$this->registerRouteTree($route);
				else:
					$msg = sprintf("Invalid route: %s does not implement %s", get_class($route),
					               PluginRouterParamsInterface::class);
					throw new InvalidArgumentException($msg);
				endif;
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Search for a route in the route tree.
	 *
	 * @param    string    $request
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Added support for regex matching.
	 * @since   3.1.0 Added support for route placeholders.
	 * @since   3.1.0 Added support for route wildcards.
	 */
	protected function _searchRouteTree(string $request) : ?string {
		// If route tree is not empty, search for a matching route in it
		if(! empty($this->_routeTree)):
			// Loop through each route in the route tree
			foreach($this->_routeTree as $key => $value):
				// Explode the route key and request into parts
				[$routeParts, $vars] = $this->_explodeRoute($key);
				$requestParts = $this->_explodeRequest($request);
				
				// Found the route which have the most matching parts
				if(( $amount = $this->_matchRequestToRoute($routeParts, $requestParts) ) > ( $found ?? 0 )):
					// Check the METHOD
					if(( $options = ( ( new Config($value) ) )->getParam('options') ) !== NULL &&
					   ( $methods = $options[RouteParams::ROUTER_PARAMS_METHOD->value] ?? NULL ) !== NULL):
						// Check if the route method is allowed
						if(in_array($this->getRouteDefault()->getRouteMethod(), $methods, TRUE) === FALSE):
							// Check if the route method is listed & return NULL if not
							break;
						endif;
					else:
						// Trigger INFO that methods should be set if special routes are used
						trigger_error('You should set allowed methods for special routes', E_USER_NOTICE);
					endif;
					
					// Save the best match so far
					$found  = $amount;
					$result = $key;
					
					// Get all the matched route parameters & their values to one array
					$matches = $this->_matchRequestVars($vars, $requestParts);
				endif;
			endforeach;
			
			// Set the route map with the matched route parameters
			if(! empty($result)):
				$this->_setRouteMap($result, $matches ?? [], $this->_findParamsFromRoutes($result));
			endif;
		endif;
		
		// Return the route name if a match is found, otherwise return NULL
		return $result ?? NULL;
	}
	
	/**
	 * @param    string    $route
	 *
	 * @return array[]
	 * @since   3.1.0 First time introduced.
	 */
	protected function _explodeRoute(string $route) : array {
		// Explode the route into segments and collect variables
		$all = $this->_explodeRequest($route);
		
		// Collect vars from the route
		foreach($all as $index => $part):
			// If the part starts with a colon, it's a variable segment
			if(strncmp($part, ':', 1) === 0):
				// Remove ':' character and store the variable name
				$vars[$index] = substr($part, 1);
			else:
				// Hard-coded section
				$parts[$index] = $part;
			endif;
		endforeach;
		
		// Return the route parts and the collected variables
		return [$parts ?? [], $vars ?? []];
	}
	
	/**
	 * @param    string    $request
	 *
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _explodeRequest(string $request) : array {
		// Trim the request string
		$request = trim($request, '/');
		
		// Remove leading slash if present
		if(strlen($request) > 1 && $request[0] === DIRECTORY_SEPARATOR):
			return explode('/', substr($request, 1));
		elseif($request !== ''):
			// Manage also empty routes
			return explode('/', $request);
		endif;
		
		return explode('/', trim($request, '/'));
	}
	
	/**
	 * @param    array    $route
	 * @param    array    $request
	 *
	 * @return int
	 * @since   3.1.0 First time introduced.
	 */
	protected function _matchRequestToRoute(array $route, array &$request) : int {
		//
		if(! empty($route) && ! empty($request)):
			// Initialize the found counter to 0
			$found = 0;
			
			// Loop through each route segment and request segment
			foreach($route as $index => $part):
				if(! empty($request[$index])):
					// Sanitize the request segment value and compare it to the route segment value
					$section = $this->_sanitizeRouteSegmentValue($request[$index]);
					
					if($part === $section):
						// Segment matches, increment the found counter
						$found++;
						
						// Remove the matched segment from the request
						unset($request[$index]);
					endif;
				else:
					// There can't be enough segments in the request
					return 0;
				endif;
			endforeach;
			
			// Return the amount of matched segments but check that at least $route parts have match
			if($found >= count($route)):
				return $found;
			endif;
		endif;
		
		//
		return 0;
	}
	
	/**
	 * @param    string    $segment
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	protected function _sanitizeRouteSegmentValue(string $segment) : ?string {
		// Use the encoding of the request if available, otherwise default to UTF-8
		if(method_exists($this, 'getEncoding')):
			$encoding = $this->getEncoding();
		else:
			$encoding = $this->_defaultEncoding;
		endif;
		
		// 1) Remove all non-alphanumeric characters, HTML tags, and scripts
		// 2) Convert to UTF-8 encoding
		// 3) Clear all HTML tags and scripts
		if(( ( $encodings = mb_detect_encoding($segment, implode(", ", mb_detect_order()), TRUE) ) !== FALSE ) &&
		   ( $converted = mb_convert_encoding($segment, $encoding, $encodings) ) !== FALSE):
			// Remove HTML tags and scripts
			$filtered = filter_var($converted, FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
			
			//
			if($filtered !== FALSE):
				return mb_strtolower($filtered, $encoding);
			endif;
		else:
			$msg = sprintf("Failed to sanitize route segment value: %s", $segment);
			throw new InvalidArgumentException($msg);
		endif;
		
		// Return null if no valid value could be found & there are errors
		return NULL;
	}
	
	/**
	 * @return null|MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteDefault() : ?MapInterface {
		return $this->_routeDefault ?? NULL;
	}
	
	/**
	 * @param    MapInterface    $map
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouteDefault(MapInterface $map) : RouterTypeHelperInterface {
		//
		$this->_routeDefault = $map;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $route
	 * @param    array    $request
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _matchRequestVars(array $route, array &$request) : ?array {
		// Try to find all variables mentioned in the route
		if(count($route) > 0):
			foreach($route as $index => $part):
				// Fill named param with null if not found
				$result[RouteParams::ROUTER_PARAMS_NAMED->value][$part] =
					$this->_checkPathVariable($request[$index] ?? NULL);
				
				// Remove the matched segment from the request
				unset($request[$index], $values);
			endforeach;
		endif;
		
		// If there are still segments in the request, they are extra segments beyond the named ones
		foreach($request as $part):
			$values[] = $this->_checkPathVariable($part);
		endforeach;
		
		// Add extra segments to the result array if any
		if(! empty($values)):
			$result[RouteParams::ROUTER_PARAMS_UNKNOWN->value] = $values;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    NULL|string    $value
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _checkPathVariable(?string $value = NULL) : ?array {
		if(! empty($value)):
			return [RouteParams::ROUTER_PARAMS_SANITIZED->value => $this->_sanitizeRouteSegmentValue($value),
			        RouteParams::ROUTER_PARAMS_RAW->value => $value,];
		endif;
		
		return NULL;
	}
	
	/**
	 * @param    string    $route
	 *
	 * @return null|PluginRouterParamsInterface
	 * @since   3.1.0 First time introduced.
	 */
	protected function _findParamsFromRoutes(string $route) : ?PluginRouterParamsInterface {
		// Check if there are routes defined
		if(! empty($this->_routes)):
			foreach($this->_routes as $plugin):
				if($plugin instanceof PluginRouterParamsInterface && $plugin->getParam('options')['route'] === $route):
					return $plugin;
				endif;
			endforeach;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    null|string    $section
	 *
	 * @return null|PluginRouterParams|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRouteMap(?string $section = NULL) : PluginRouterParams|array|null {
		// Check if route map is available and $section is valid
		if($section === NULL || in_array($section, self::ROUTE_MAP_KEYS) === TRUE):
			// Return the entire route map if $section is NULL or a valid key
			#var_dump($this->_routeMap);
			return match ( $section ) {
				RouteParams::ROUTER_PARAMS_KEY->value => $this->_routeMap[RouteParams::ROUTER_PARAMS_KEY->value] ??
				                                         NULL,
				RouteParams::ROUTER_PARAMS_MATCHES->value => $this->_routeMap[RouteParams::ROUTER_PARAMS_MATCHES->value]
				                                             ?? NULL,
				RouteParams::ROUTER_PARAMS_OPTIONS->value => $this->_routeMap[RouteParams::ROUTER_PARAMS_OPTIONS->value]
				                                             ?? NULL,
				default => $this->_routeMap ?? NULL,
			};
		else:
			$msg = sprintf("Invalid section '%s' for route map. Section must be one of %s.", $section,
			               implode(', ', self::ROUTE_MAP_KEYS));
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    string                                    $key
	 * @param    array                                     $matches
	 * @param    null|array|PluginRouterParamsInterface    $options
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Added support for associative arrays in options.
	 */
	protected function _setRouteMap(string $key, array $matches, PluginRouterParamsInterface|array|null $options = NULL) : void {
		//
		$this->_routeMap =
			[RouteParams::ROUTER_PARAMS_KEY->value => $key, RouteParams::ROUTER_PARAMS_MATCHES->value => $matches,
			 RouteParams::ROUTER_PARAMS_OPTIONS->value => $options];
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|PluginRouterParams
	 * @since   3.0.0 First time introduced.
	 */
	public function getRoute(string $name) : ?PluginRouterParams {
		return $this->_routes[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteRoute(string $name) : RouterTypeHelperInterface {
		//
		unset($this->_routes[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRoutes() : RouterTypeHelperInterface {
		//
		$this->_routes = [];
		
		//
		return $this;
	}
	
	/**
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouteDefault() : RouterTypeHelperInterface {
		//
		unset($this->_routeDefault);
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $childs
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addRouteChilds(array $childs) : RouterTypeHelperInterface {
		if(! empty($childs)):
			foreach($childs as $key => $val):
				$this->addRouteChild($key, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    RouteParams|string    $name
	 * @param    MapInterface          $map
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addRouteChild(RouteParams|string $name, MapInterface $map) : RouterTypeHelperInterface {
		//
		$this->_routeChild[] = $map;
		
		//
		return $this;
	}
	
	/**
	 * Return all child routes
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteChilds() : ?array {
		return $this->_routeChild ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteChild(string $name) : ?MapInterface {
		return $this->_routeChild[$name] ?? NULL;
	}
	
	/**
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouteChilds() : RouterTypeHelperInterface {
		//
		unset($this->_routeChild);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouteChild(string $name) : RouterTypeHelperInterface {
		//
		unset($this->_routeChild[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $segment
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _detectFromPathVariable(string $segment) : bool {
		return str_starts_with($segment, ':');
	}
	
	/**
	 * Detect if the segment is a path option (like [id] or [:action])
	 *
	 * @param    string    $segment
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _detectFromPathOption(string $segment) : bool {
		return $segment[0] === '[' && $segment[strlen($segment) - 1] === ']';
	}
}
