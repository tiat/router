<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Helper;

//
use Tiat\Router\Router\MapInterface;
use Tiat\Router\Router\RouteParams;
use Tiat\Router\Router\Type\Plugin\PluginRouterParams;
use Tiat\Router\Router\Type\Plugin\PluginRouterParamsInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RouterTypeHelperInterface {
	
	/**
	 * Define the route map keys which should be checked during matching.
	 *
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	public const array ROUTE_MAP_KEYS = [RouteParams::ROUTER_PARAMS_KEY->value,
	                                     RouteParams::ROUTER_PARAMS_MATCHES->value,
	                                     RouteParams::ROUTER_PARAMS_OPTIONS->value];
	
	/**
	 * @return null|MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteDefault() : ?MapInterface;
	
	/**
	 * @param    MapInterface    $map
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouteDefault(MapInterface $map) : RouterTypeHelperInterface;
	
	/**
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouteDefault() : RouterTypeHelperInterface;
	
	/**
	 * @param    array    $childs
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function addRouteChilds(array $childs) : RouterTypeHelperInterface;
	
	/**
	 * @param    RouteParams     $name
	 * @param    MapInterface    $map
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function addRouteChild(RouteParams $name, MapInterface $map) : RouterTypeHelperInterface;
	
	/**
	 * Return all child routes
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteChilds() : ?array;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteChild(string $name) : ?MapInterface;
	
	/**
	 * @return RouterTypeHelperInterface
	 */
	public function resetRouteChilds() : RouterTypeHelperInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return RouterTypeHelperInterface
	 */
	public function resetRouteChild(string $name) : RouterTypeHelperInterface;
	
	/**
	 * @param    string    $name
	 * @param    array     $params
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addRoute(string $name, array $params) : RouterTypeHelperInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|PluginRouterParams
	 * @since   3.0.0 First time introduced.
	 */
	public function getRoute(string $name) : ?PluginRouterParams;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRoutes() : ?array;
	
	/**
	 * @param    string    $name
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteRoute(string $name) : RouterTypeHelperInterface;
	
	/**
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRoutes() : RouterTypeHelperInterface;
	
	/**
	 * @param    string    $route    The route to be searched.
	 *
	 * @return PluginRouterParams|null  The parameters of the found route, or null if not found.
	 * @since   3.0.0 First time introduced.
	 */
	public function searchRoute(string $route) : ?PluginRouterParams;
	
	/**
	 * @param    bool    $status
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setCheckRoutes(bool $status) : RouterTypeHelperInterface;
	
	/**
	 * @param    PluginRouterParamsInterface    $params
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function registerRouteTree(PluginRouterParamsInterface $params) : RouterTypeHelperInterface;
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function isCheckRoutesEnabled() : bool;
	
	/**
	 * @param    array    $routes
	 *
	 * @return RouterTypeHelperInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Added the ability to register routes from an array.
	 */
	public function registerRoutes(array $routes) : RouterTypeHelperInterface;
	
	/**
	 * @param    null|string    $section
	 *
	 * @return null|PluginRouterParams|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRouteMap(string $section = NULL) : PluginRouterParams|array|null;
}
