<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type;

//
use Tiat\Mvc\DispatchManager;
use Tiat\Mvc\DispatchManagerInterface;
use Tiat\Router\Exception\RuntimeException;
use Tiat\Router\Router\MapInterface;
use Tiat\Router\Router\Type\Helper\RouterTypeHelperInterface;
use Tiat\Router\Router\Type\Helper\RouterTypeHelperTrait;
use Tiat\Router\Router\Type\Plugin\RouterPluginInterface;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Loader\ClassLoaderTrait;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function is_string;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractRouterType implements ClassLoaderInterface, ParametersPluginInterface, RouterTypeInterface, RouterTypeHelperInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_DISPATCH_MANAGER = DispatchManager::class;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use RouterTypeHelperTrait;
	
	/**
	 * @var RouterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	private RouterPluginInterface $_routerPlugin;
	
	/**
	 * @var DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	private DispatchManagerInterface $_dispatchManager;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_routeChilds;
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = FALSE, bool $autorun = FALSE, ...$args) {
		//
		$this->_defineAutostart($autoinit, $autorun, ...$args);
		
		//
		if(! empty($params)):
			$this->setParams($params);
		endif;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	final public function __invoke() : static {
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouterPlugin() : static {
		//
		unset($this->_routerPlugin);
		
		//
		return $this;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function matchRoute() : void {
		//
		if(( $default = $this->getRouteDefault() ) !== NULL):
			// Set default route
			$route =
				$this->getRouterPlugin()?->setRouteDefault($default)->addRouteChilds((array)$this->getRouteChilds())
				     ->resolve();
			
			// Declare dispatcher
			if($route !== NULL):
				$this->declareDispatchManager($route, $this->getDispatchManager())->run();
			endif;
		else:
			throw new RuntimeException("No default route.");
		endif;
	}
	
	/**
	 * @return null|MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteDefault() : ?MapInterface {
		return $this->_routeDefault ?? NULL;
	}
	
	/**
	 * @return null|RouterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouterPlugin() : ?RouterPluginInterface {
		return $this->_routerPlugin ?? NULL;
	}
	
	/**
	 * @param    RouterPluginInterface|string    $plugin
	 * @param    null|array                      $settings
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouterPlugin(RouterPluginInterface|string $plugin, ?array $settings = NULL) : static {
		//
		if(is_string($plugin)):
			// Create new empty plugin without settings
			$plugin = new $plugin($settings);
		endif;
		
		// Set (new) plugin with settings
		$this->_routerPlugin = $plugin;
		
		//
		return $this;
	}
	
	/**
	 * @param    MapInterface                $map
	 * @param    DispatchManagerInterface    $manager
	 *
	 * @return DispatchManagerInterface
	 */
	public function declareDispatchManager(MapInterface $map, DispatchManagerInterface $manager) : DispatchManagerInterface {
		return $manager->setRouteMap($map);
	}
	
	/**
	 * @return null|DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getDispatchManager() : ?DispatchManagerInterface {
		//
		if(empty($this->_dispatchManager)):
			$this->setDispatchManager();
		endif;
		
		//
		return $this->_dispatchManager ?? NULL;
	}
	
	/**
	 * @param    DispatchManagerInterface|string    $manager
	 *
	 * @return AbstractRouterType
	 * @since   3.0.0 First time introduced.
	 */
	public function setDispatchManager(DispatchManagerInterface|string $manager = self::DEFAULT_DISPATCH_MANAGER) : static {
		//
		if(is_string($manager)):
			$this->_dispatchManager = new $manager();
		else:
			$this->_dispatchManager = $manager;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    MapInterface    $map
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function addRouteDefault(MapInterface $map) : static {
		//
		$this->_routeDefault = $map;
		
		//
		return $this;
	}
}
