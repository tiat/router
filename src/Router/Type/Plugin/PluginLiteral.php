<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Plugin;

//
use Tiat\Router\Router\MapInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Added support for route parameters.
 */
class PluginLiteral extends AbstractRouterPlugin {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string ROUTER_TYPE = 'literal';
	
	/**
	 * Routes a literal path and updates the provided map based on the route parameters.
	 *
	 * @param    MapInterface                        $map      The map that defines the routing structure.
	 * @param    null|PluginRouterParamsInterface    $route    The route parameter interface containing options.
	 *
	 * @return null|MapInterface Returns the updated map or null if no options were provided or the map isn't modified.
	 * @since   3.1.0 First time introduced.
	 */
	public function modifyRouteAction(MapInterface $map, ?PluginRouterParamsInterface $route = NULL) : ?MapInterface {
		return $this->replaceMapWithRoute($map, $route);
	}
}
