<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Plugin;

//
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum RouterPluginType: string implements InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case LITERAL = 'literal';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case METHOD = 'method';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case SEGMENT = 'segment';
}
