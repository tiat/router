<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Plugin;

//
use Tiat\Router\Exception\BadMethodCallException;
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * luginRouterParams class is used to manage the routing parameters for plugins.
 *  It provides a structured way to handle routing-related configuration and options.
 *  Example usage:
 *     - $class->getUid();  // Fetches the UID parameter.
 *     - $class->getMyParameter();  // Fetches the 'myParameter' parameter.
 * @method mixed get{name}() Dynamically fetches any named parameter, based on the 'name' suffix in the method name.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface PluginRouterParamsInterface extends ParametersPluginInterface {
	
	/**
	 * @param    string    $method
	 * @param    array     $args
	 *
	 * @return mixed
	 * @method mixed get{name}() Dynamically fetches any named parameter, based on the 'name' suffix in the method name.
	 * @since   3.1.1 First time introduced.
	 */
	public function __call(string $method, array $args) : mixed;
	
	/**
	 * Checks if the specified method exists in the named parameters.
	 *
	 * @param    string    $method    The name of the method to check.
	 *
	 * @return bool Returns true if the method exists in the named parameters, false otherwise.
	 * @since   3.1.1 First time introduced.
	 */
	public function hasMethod(string $method) : bool;
	
	/**
	 * Extracts the property name from a method call that follows the naming convention 'get{PropertyName}'.
	 * This method checks if the provided method name starts with 'get' and converts the rest of the method name
	 * to lowercase, which is used for dynamically fetching related parameters.
	 *
	 * @param    string    $method    The name of the method being called, expected to start with 'get'.
	 *
	 * @return string The extracted and formatted property name from the method call.
	 * @throws BadMethodCallException If the method name does not start with 'get'.
	 * @since   3.1.1 First time introduced.
	 */
	public static function getMethodName(string $method) : string;
}