<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Plugin;

//
use Tiat\Router\Router\MapInterface;
use Tiat\Router\Router\Type\Helper\RouterTypeHelperInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RouterPluginInterface extends RouterTypeHelperInterface {
	
	/**
	 * This method checks the route map and returns an instance that implements the MapInterface.
	 *
	 * @return MapInterface|null Returns the MapInterface instance if the route map exists, otherwise null.
	 * @since   3.0.0 First time introduced.
	 */
	public function checkRouteMap() : ?MapInterface;
	
	/**
	 * This method resolves the necessary map and returns an instance that implements the MapInterface.
	 *
	 * @return MapInterface|null Returns the MapInterface instance if available, otherwise null.
	 * @since   3.0.0 First time introduced.
	 */
	public function resolve() : ?MapInterface;
	
	/**
	 * Modifies the route within the given map.
	 *
	 * @param    MapInterface                   $map      The map object to be modified.
	 * @param    PluginRouterParamsInterface    $route    The route parameters to apply, or null for default behavior.
	 *
	 * @return MapInterface|null The modified map object, or null if no modifications were applied.
	 * @since   3.1.0 First time introduced.
	 */
	public function modifyRoute(MapInterface $map, PluginRouterParamsInterface $route) : ?MapInterface;
}
