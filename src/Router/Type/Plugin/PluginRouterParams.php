<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Plugin;

//
use Tiat\Router\Exception\BadMethodCallException;
use Tiat\Router\Router\RouteParams;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function array_key_exists;
use function lcfirst;
use function sprintf;
use function strncmp;
use function substr;

/**
 * PluginRouterParams class is used to manage the routing parameters for plugins.
 * It provides a structured way to handle routing-related configuration and options.
 * Example usage:
 *    - $class->getUid();  // Fetches the UID parameter.
 *    - $class->getMyParameter();  // Fetches the 'myParameter' parameter.
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class PluginRouterParams implements PluginRouterParamsInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @param    string    $_name
	 * @param    array     $params
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(private string $_name, array $params) {
		//
		$this->setParams($params);
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string {
		return $this->_name;
	}
	
	/**
	 * Handles dynamic method calls to retrieve named parameters from the PATH request.
	 *
	 * @param    string    $method    The name of the called method.
	 * @param    array     $args      The arguments passed to the called method.
	 *
	 * @return   mixed     The value of the named parameter if it exists, or null if not found.
	 * @throws   BadMethodCallException    If the called method does not exist.
	 * @since   3.1.1 First time introduced.
	 */
	public function __call(string $method, array $args) : mixed {
		// Check if the method exists in the named parameters
		if($this->hasMethod($method) === TRUE):
			// Get named parameters from the router parameters
			$params = $this->getParams()[RouteParams::ROUTER_PARAMS_NAMED->value];
			
			// Return the value of the named parameter if it exists, or null if not found
			return $params[self::getMethodName($method)] ?? NULL;
		endif;
		
		// Throw a BadMethodCallException if the method name does not start with 'get'
		self::_getBadMethodCallException($method);
	}
	
	/**
	 * Checks if the specified method exists in the named parameters.
	 *
	 * @param    string    $method    The name of the method to check.
	 *
	 * @return bool Returns true if the method exists in the named parameters, false otherwise.
	 * @since   3.1.1 First time introduced.
	 */
	public function hasMethod(string $method) : bool {
		// Get named parameters from the router parameters
		$params = $this->getParams()[RouteParams::ROUTER_PARAMS_NAMED->value];
		
		// Check if the method exists in the named parameters and return the result
		return array_key_exists(self::getMethodName($method), $params);
	}
	
	/**
	 * Extracts the property name from a method call that follows the naming convention 'get{PropertyName}'.
	 * This method checks if the provided method name starts with 'get' and converts the rest of the method name
	 * to lowercase, which is used for dynamically fetching related parameters.
	 *
	 * @param    string    $method    The name of the method being called, expected to start with 'get'.
	 *
	 * @return string The extracted and formatted property name from the method call.
	 * @throws BadMethodCallException If the method name does not start with 'get'.
	 * @since   3.1.1 First time introduced.
	 */
	public static function getMethodName(string $method) : string {
		if(strncmp($method, 'get', 3) === 0):
			// Extract the property name from the method call (e.g., 'getUid' -> 'UID')
			// Convert 'getUid' to 'uid'
			// This will allow also getter like getMyParameter() if it's named in the router parameters
			return lcfirst(substr($method, 3));
		endif;
		
		// Throw a BadMethodCallException if the method name does not start with 'get'
		self::_getBadMethodCallException($method);
	}
	
	private static function _getBadMethodCallException(string $method) : BadMethodCallException {
		// Throw a BadMethodCallException if the method does not exist
		$msg = sprintf('Call to undefined method %s::%s()', static::class, $method);
		throw new BadMethodCallException($msg);
	}
}