<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Plugin;

//
use Jantia\Asi\Register\AsiRegisterHelperInterface;
use ReflectionClass;
use Tiat\Router\Router\MapInterface;
use Tiat\Router\Router\RouteParams;
use Tiat\Router\Router\Type\Helper\RouterTypeHelperTrait;

/**
 * @version 3.1.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Added support for route parameters.
 * @since   3.1.0 Added replaceMapWithRoute() which replaces the map with the modified route by application.
 */
abstract class AbstractRouterPlugin implements AsiRegisterHelperInterface, RouterPluginInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use RouterTypeHelperTrait;
	
	/**
	 * @return null|MapInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resolve() : ?MapInterface {
		// Try to find router settings and map
		if(( $map = $this->checkRouteMap() ) !== NULL):
			return $map;
		else:
			// Modify the route if no match is found with router type and map
			return $this->modifyRoute($this->getRouteDefault());
		endif;
	}
	
	/**
	 * @return null|MapInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Added support for route parameters.
	 */
	public function checkRouteMap() : ?MapInterface {
		// Search for the route in the map
		if(( $router = $this->searchRoute(( $map = $this->getRouteDefault() )->getQuery()) ) !== NULL):
			return $this->modifyRoute($map, $router);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * Modifies the provided route based on the given map and route parameters.
	 *
	 * @param    MapInterface                        $map      The map that defines the routing structure.
	 * @param    null|PluginRouterParamsInterface    $route    The route parameter interface to apply modifications.
	 *
	 * @return null|MapInterface Returns the modified map or null if modification fails.
	 * @since   3.1.0 First time introduced.
	 */
	public function modifyRoute(MapInterface $map, ?PluginRouterParamsInterface $route = NULL) : ?MapInterface {
		// Get router type from route parameters
		if($route === NULL || ( $type = $route->getParam(RouteParams::ROUTER_PARAMS_TYPE->value) ) === NULL):
			// Otherwise try to find the router type from this class itself
			$type = ( new ReflectionClass($this) )->getConstant('ROUTER_TYPE');
		endif;
		
		// Handle router type
		if($type === PluginLiteral::ROUTER_TYPE):
			// Type Literal is default, so use the map directly
			return ( new PluginLiteral() )->modifyRouteAction($map, $route);
		elseif($type === PluginSegment::ROUTER_TYPE):
			// We can push route to Literal router type & handle variables & segments as well
			return ( new PluginSegment() )->modifyRouteAction($map, $route, $this->_getParentPluginType());
		elseif($type === PluginMethod::ROUTER_TYPE):
			// Method router type, so use the map directly
			#var_dump($map);
			return ( new PluginMethod() )->modifyRouteAction($map, $route);
		endif;
		
		//
		return $map;
	}
	
	/**
	 * Retrieves the parent plugin type for the current class.
	 *
	 * @return false|string Returns the plugin type as a string or false if the constant 'ROUTER_TYPE' is not defined.
	 * @since   3.1.0 First time introduced.
	 */
	protected function _getParentPluginType() : false|string {
		return ( new ReflectionClass($this) )->getConstant('ROUTER_TYPE');
	}
	
	/**
	 * Replaces the default controller and action in the map based on the provided route parameters.
	 *
	 * @param    MapInterface                        $map      The map that defines the routing structure.
	 * @param    null|PluginRouterParamsInterface    $route    The route parameter interface used to obtain default values.
	 *
	 * @return null|MapInterface Returns the updated map or null if the replacement fails.
	 * @since   3.1.0 First time introduced.
	 */
	public function replaceMapWithRoute(MapInterface $map, ?PluginRouterParamsInterface $route = NULL) : ?MapInterface {
		// Get options & replace default controller & action in the map
		if($route !== NULL && ( ( $options = $route->getParam(RouteParams::ROUTER_PARAMS_OPTIONS->value) ) !== NULL ) &&
		   ! empty($default = $options[RouteParams::ROUTER_PARAMS_DEFAULTS->value] ?? [])):
			
			#var_dump($default);
			
			// Get controller & action from default options
			$controller = $default[RouteParams::ROUTER_PARAMS_CONTROLLER->value] ?? NULL;
			$action     = $default[RouteParams::ROUTER_PARAMS_ACTION->value] ?? NULL;
			
			// Update the map with new module & controller name
			$map->replaceModuleWithControllerName($controller);
			
			// Update the map with the default controller and action
			if($action !== NULL):
				$map->replaceActionName($action);
			endif;
		endif;
		
		// If route has params resolved from the PATH, add them to the Map
		if($route !== NULL):
			// Add founded params to the Map anyway.
			if(( $params = $route->getParam(RouteParams::ROUTER_PARAMS_MATCHES->value) ) !== NULL):
				$map->addVariables(new PluginRouterParams('path', $params));
			endif;
			
			// Check if route has constraints
			if(( $rules = $route->getParam(RouteParams::ROUTER_PARAMS_CONSTRAINTS->value) ) !== NULL):
				$map->addConstraints(new PluginRouterParams('contraints', $rules));
			endif;
		endif;
		
		//
		return $map;
	}
}
