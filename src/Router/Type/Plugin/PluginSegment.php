<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Plugin;

use Jantia\Asi\Register\AsiRegisterHelper;
use Tiat\Router\Exception\InvalidArgumentException;
use Tiat\Router\Router\MapInterface;

use function sprintf;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class PluginSegment extends AbstractRouterPlugin {
	
	//
	use AsiRegisterHelper;
	
	/**
	 * The type of router.
	 *
	 * @var string ROUTER_TYPE
	 * @since   3.1.0 First time introduced.
	 */
	public const string ROUTER_TYPE = 'segment';
	
	/**
	 * Modifies the given route action based on the specified map and route parameters.
	 *
	 * @param    null|MapInterface                   $map             The map to modify, can be NULL if not provided.
	 * @param    null|PluginRouterParamsInterface    $route           The route parameters, can be NULL.
	 * @param    null|string                         $sourcePlugin    The source plugin to apply; an exception is thrown if it is not 'method'.
	 *
	 * @return MapInterface|null The modified map or NULL if modifications were not applied.
	 * @since   3.1.0 First time introduced.
	 */
	public function modifyRouteAction(?MapInterface $map = NULL, ?PluginRouterParamsInterface $route = NULL, ?string $sourcePlugin = NULL) : ?MapInterface {
		// Set map first with source plugin
		if($sourcePlugin === 'method'):
			$map = ( new PluginMethod() )->modifyRouteAction($map);
		else:
			$msg = sprintf("Implementation is missing for router plugin %s.", $sourcePlugin);
			throw new InvalidArgumentException($msg);
		endif;
		
		// Modify route with custom router logic
		if($route !== NULL):
			$map = $this->replaceMapWithRoute($map, $route);
		endif;
		
		#&& ($route->getParam(RouteParams::ROUTER_PARAMS_OPTIONS->value)) !== NULL
		
		// Return the modified map
		return $map;
	}
}