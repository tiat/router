<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type\Plugin;

//
use Jantia\Asi\Register\AsiRegisterHelper;
use Jantia\Asi\Register\AsiRegisterModules;
use Psr\Http\Message\RequestInterface;
use Tiat\Router\Router\MapInterface;

/**
 * @version 3.1.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Added support for route parameters.
 */
class PluginMethod extends AbstractRouterPlugin {
	
	//
	use AsiRegisterHelper;
	
	/**
	 * The type of router.
	 *
	 * @var string ROUTER_TYPE
	 * @since   3.0.0 First time introduced.
	 */
	public const string ROUTER_TYPE = 'method';
	
	/**
	 * Modifies the route action in the given map object based on the request method obtained from MessageBus.
	 * If a map object is provided and the default message bus and request message are available, this method
	 * retrieves the request object from the message and checks if it is an instance of RequestInterface. If so,
	 * it gets the request method and replaces the action name in the map with the method.
	 *
	 * @param    null|MapInterface    $map    The map object to modify the route action in.
	 *
	 * @return MapInterface|null The modified map object if available, otherwise null.
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Added support for route parameters.
	 */
	public function modifyRouteAction(?MapInterface $map = NULL, ?PluginRouterParamsInterface $route = NULL) : ?MapInterface {
		// Get the request interface from MessageBus
		if($route !== NULL):
			// Modify route first with methods & then with router parameters
			return $this->replaceMapWithRoute($this->modifyRouteAction($map), $route);
		elseif($map !== NULL && ( $bus = $this->getMessageBusDefault() ) !== NULL &&
		       ( $r = $bus->getMessage(AsiRegisterModules::REQUEST->value) ) !== NULL):
			//
			$request = $r->getData();
			
			//
			if($request instanceof RequestInterface):
				$method = $request->getMethod();
				$map->replaceActionName($method);
			endif;
		endif;
		
		//
		return $map;
	}
}
