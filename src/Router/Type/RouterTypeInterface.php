<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router\Type;

//
use Tiat\Mvc\DispatchManagerInterface;
use Tiat\Router\Router\Type\Plugin\RouterPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RouterTypeInterface {
	
	/**
	 * Get router plugin.
	 *
	 * @return null|RouterPluginInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouterPlugin() : ?RouterPluginInterface;
	
	/**
	 * Reset router plugin.
	 *
	 * @return RouterTypeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouterPlugin() : RouterTypeInterface;
	
	/**
	 * Set router plugin (mandatory).
	 *
	 * @param    RouterPluginInterface|string    $plugin
	 * @param    array                           $settings
	 *
	 * @return RouterTypeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouterPlugin(RouterPluginInterface|string $plugin, array $settings) : RouterTypeInterface;
	
	/**
	 * @return null|DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getDispatchManager() : ?DispatchManagerInterface;
	
	/**
	 * @param    DispatchManagerInterface    $manager
	 *
	 * @return RouterTypeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDispatchManager(DispatchManagerInterface $manager) : RouterTypeInterface;
}
