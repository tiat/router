<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router;

//
use Jantia\Standard\Platform\PlatformEnv;
use Tiat\Router\Router\Type\Plugin\PluginRouterParamsInterface;
use Tiat\Router\Std\RouterDefault;
use Tiat\Router\Std\RouterPart;
use Tiat\Standard\DataModel\HttpMethod;

use function array_map;
use function array_slice;
use function getenv;
use function implode;
use function strlen;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.1 Addition of RouteMethod
 */
class Map extends AbstractMap {
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_nameNamespace;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_nameModule;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_nameController;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_nameControllerBase;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_nameAction;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_language;
	
	/**
	 * @var HttpMethod
	 * @since   3.1.1 First time introduced.
	 */
	private HttpMethod $_routeMethod;
	
	/**
	 * @var PluginRouterParamsInterface
	 * @since   3.1.0 First time introduced.
	 */
	private PluginRouterParamsInterface $_variables;
	
	/**
	 * @var PluginRouterParamsInterface
	 * @since   3.1.0 First time introduced.
	 */
	private PluginRouterParamsInterface $_constraints;
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function toArray() : array {
		return $this->__toArray();
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function __toArray() : array {
		return [RouterPart::NAMESPACE->value => $this->getNamespaceName(),
		        RouterPart::CONTROLLER->value => $this->getControllerName(),
		        RouterPart::ACTION->value => $this->getActionName(),];
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getNamespaceName() : ?string {
		// Launch the trigger
		if(empty($this->_nameNamespace)):
			$this->_nameNamespace = $this->_setNamespaceName();
		endif;
		
		//
		return $this->_nameNamespace ?? NULL;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setNamespaceName() : string {
		//
		$ns  = $this->getDefaultPartNamespace();
		$mod = $this->getDefaultPartModule();
		$con = $this->getDefaultPartController();
		
		// Format namespace name
		if(! empty($route = $this->getRouteResolved())):
			// Get namespace & module sections from route
			$add = array_map(function ($value) {
				return $this->_ucfirst($value);
			}, array_slice($route, 0, 2));
		else:
			$add[] = $this->_checkRouteMapValue($ns->value,
				( getenv(PlatformEnv::ENV_NAMESPACE->value) ?? RouterDefault::fromRouterPart($ns)->value ));
			$add[] = $this->_formatName(RouterDefault::fromRouterPart($mod)->value);
		endif;
		
		// Add module name at same because it's resolved
		$this->_updateModuleName($this->_setModuleName($add[$this->getDefaultPartModule()->value ?? NULL]));
		
		// Format controller name
		$add[] = $this->_formatName($con->value);
		
		//
		return implode('\\', $add);
	}
	
	/**
	 * @param    string    $module
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	protected function _updateModuleName(string $module) : static {
		//
		$this->_routeCurrent[RouterPart::MODULE->value] = ( $this->_nameModule = $module );
		
		//
		return $this;
	}
	
	/**
	 * @param    null|string    $name
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setModuleName(?string $name = NULL) : string {
		if(! empty($name)):
			$add[] = $this->_formatName($name);
		elseif(! empty($route = $this->getRoute())):
			$add = array_map(function ($value) {
				return $this->_ucfirst($value);
			}, array_slice($route, 1, 1));
		else:
			$mod   = $this->getDefaultPartModule();
			$add[] = $this->_formatName(RouterDefault::fromRouterPart($mod)->value);
		endif;
		
		//
		return implode('', $add);
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerName() : ?string {
		// Launch the trigger
		if(empty($this->_nameController)):
			$this->_updateControllerName($this->_setControllerName());
		endif;
		
		//
		return $this->_nameController ?? NULL;
	}
	
	/**
	 * @param    string    $controller
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	protected function _updateControllerName(string $controller) : static {
		//
		$this->_routeCurrent[RouterPart::CONTROLLER->value] = ( $this->_nameController = $controller );
		
		//
		return $this;
	}
	
	/**
	 * @param    null|string    $controller
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setControllerName(?string $controller = NULL) : string {
		//
		$part = $this->getDefaultPartController();
		
		//
		if(! empty($controller)):
			$add[] = $this->_ucfirst($controller);
		elseif(! empty($route = $this->getRouteResolved())):
			$add = array_map(function ($value) {
				return $this->_ucfirst($value);
			}, array_slice($route, 2, 1));
		else:
			$add[] = $this->_checkRouteMapValue($part, RouterDefault::fromRouterPart($part)->value);
		endif;
		
		// Set controller base name
		$this->_setControllerBaseName(implode('', array_values($add)));
		
		//
		$add[] = $this->_formatName($part->value);
		
		//
		return implode('', $add);
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setControllerBaseName(string $name) : void {
		$this->_nameControllerBase = $name;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getActionName() : ?string {
		// Launch the trigger
		if(empty($this->_nameAction)):
			$this->_updateActionName($this->_setActionName());
		endif;
		
		//
		return $this->_nameAction ?? NULL;
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return $this
	 * @since 3.1.0 First time introduced.
	 */
	protected function _updateActionName(string $action) : static {
		//
		$this->_routeCurrent[RouterPart::ACTION->value] = ( $this->_nameAction = $action );
		
		//
		return $this;
	}
	
	/**
	 * @param    null|string    $action
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setActionName(?string $action = NULL) : string {
		//
		$part = $this->getDefaultPartAction();
		
		//
		if(! empty($action)):
			$add[] = mb_strtolower($action);
		elseif(! empty($route = $this->getRouteResolved())):
			$add[] = $route[$part->value];
		else:
			$add[] = RouterDefault::fromRouterPart($part)->value;
		endif;
		
		//
		$add[] = $this->_formatName($part->value);
		
		//
		return implode('', $add);
	}
	
	/**
	 * @param    string    $fullControllerName
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function replaceModuleWithControllerName(string $fullControllerName) : static {
		//
		$parts = explode('\\', $fullControllerName);
		
		// Remove the first part (namespace)
		// In version 3.0.0, the namespace is not possible to replace directly so it will be removed.
		array_shift($parts);
		
		//
		$this->replaceModuleName(array_shift($parts));
		array_shift($parts);
		
		$controller = array_shift($parts);
		if(str_ends_with($controller,
				( $default = $this->_formatName(( $this->getDefaultPartController() )->value) )) === TRUE):
			$this->replaceControllerName(substr($controller, 0, -strlen($default)));
		else:
			$this->replaceControllerName($controller);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $module
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function replaceModuleName(string $module) : static {
		return $this->_updateModuleName($this->_setModuleName($module));
	}
	
	/**
	 * @param    string    $controller
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function replaceControllerName(string $controller) : static {
		return $this->_updateControllerName($this->_setControllerName($controller));
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function replaceActionName(string $action) : static {
		return $this->_updateActionName($this->_setActionName($action));
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getLanguage() : ?array {
		return $this->_language ?? NULL;
	}
	
	/**
	 * @param    array|string    $language
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setLanguage(array|string $language = 'en') : static {
		//
		if(! empty($language)):
			$this->_language = (array)$language;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerBaseName() : ?string {
		// Launch the trigger
		if(empty($this->_nameControllerBase)):
			$this->_nameNamespace = $this->_setNamespaceName();
		endif;
		
		//
		return $this->_nameControllerBase ?? NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getModuleName() : ?string {
		if(empty($this->_nameModule)):
			$this->_nameModule = $this->_setModuleName();
		endif;
		
		//
		return $this->_nameModule ?? NULL;
	}
	
	/**
	 * @param    PluginRouterParamsInterface    $params
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function addVariables(PluginRouterParamsInterface $params) : static {
		//
		$this->_variables = $params;
		
		//
		return $this;
	}
	
	/**
	 * @return null|PluginRouterParamsInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getVariables() : ?PluginRouterParamsInterface {
		return $this->_variables ?? NULL;
	}
	
	/**
	 * @param    PluginRouterParamsInterface    $constraints
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function addConstraints(PluginRouterParamsInterface $constraints) : static {
		//
		$this->_constraints = $constraints;
		
		//
		return $this;
	}
	
	/**
	 * @return null|PluginRouterParamsInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getConstraints() : ?PluginRouterParamsInterface {
		return $this->_constraints ?? NULL;
	}
	
	/**
	 * @return null|HttpMethod
	 * @since   3.1.1 First time introduced.
	 */
	public function getRouteMethod() : ?HttpMethod {
		return $this->_routeMethod ?? NULL;
	}
	
	/**
	 * @param    HttpMethod    $method
	 *
	 * @return $this
	 * @since   3.1.1 First time introduced.
	 */
	public function setRouteMethod(HttpMethod $method) : static {
		//
		$this->_routeMethod = $method;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setNamespaceNameForError() : string {
		//
		$ns  = $this->getDefaultPartNamespace();
		$mod = $this->getDefaultPartError();
		$con = $this->getDefaultPartController();
		
		//
		$add[] = $this->_checkRouteMapValue($ns->value,
			( getenv(PlatformEnv::ENV_NAMESPACE->value) ?? RouterDefault::fromRouterPart($ns)->value ));
		$add[] = $this->_formatName(RouterDefault::fromRouterPart($mod)->value);
		$add[] = $this->_formatName($con->value);
		
		//
		return implode('\\', $add);
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setControllerNameForError() : string {
		return $this->_setControllerName(RouterDefault::fromRouterPart($this->getDefaultPartController())->value);
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setActionNameForError() : string {
		return $this->_setActionName(RouterDefault::fromRouterPart($this->getDefaultPartAction())->value);
	}
}
