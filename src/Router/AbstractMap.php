<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router;

//
use Jantia\Standard\Platform\PlatformEnv;
use Jantia\Stdlib\Message\Message;
use Psr\Log\LogLevel;
use Tiat\Router\Exception\RuntimeException;
use Tiat\Router\Router\Plugin\PluginMapInterface;
use Tiat\Router\Router\Plugin\PluginMapSettings;
use Tiat\Router\Router\Plugin\PluginMapSettingsInterface;
use Tiat\Router\Router\Std\RouterPartInterface;
use Tiat\Router\Router\Std\RouterPartTrait;
use Tiat\Router\Std\RouterDefault;
use Tiat\Router\Std\RouterPart;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function array_combine;
use function array_is_list;
use function array_keys;
use function array_map;
use function array_slice;
use function array_values;
use function count;
use function ctype_alnum;
use function ctype_digit;
use function explode;
use function getenv;
use function implode;
use function is_array;
use function is_scalar;
use function is_string;
use function mb_strtolower;
use function preg_replace;
use function sprintf;
use function str_replace;
use function str_starts_with;
use function strtolower;
use function substr;
use function ucwords;

/**
 * Will return the route as string (default: /module/controller/view)
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractMap extends Message implements MapInterface, ParametersPluginInterface, RouterPartInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use RouterPartTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const array DEFAULT_ROUTE_TREE = [RouterPart::MODULE->value => self::DEFAULT_MODULE_PART,
	                                         RouterPart::CONTROLLER->value => self::DEFAULT_CONTROLLER_PART,
	                                         RouterPart::VIEW->value => self::DEFAULT_ACTION_PART];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string WARNING_ROUTE_TOO_MANY_SECTIONS = 'route_has_too_many_sections';
	
	/**
	 * Current route sections
	 *
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_routeCurrent;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_defaultRouteTree = self::DEFAULT_ROUTE_TREE;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private readonly array $_routeResolved;
	
	/**
	 * Query string from the request
	 *
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private readonly string $_query;
	
	/**
	 * Error route
	 *
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private readonly array $_error;
	
	/**
	 * @var PluginMapSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	private PluginMapSettingsInterface $_settings;
	
	/**
	 * @var array|string[][]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_templates = [LogLevel::WARNING => [self::WARNING_ROUTE_TOO_MANY_SECTIONS => "Route has more sections than default route tree."]];
	
	/**
	 * @param    PluginMapSettingsInterface|string|array    $route
	 * @param                                               ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(PluginMapSettingsInterface|string|array $route, ...$args) {
		//
		parent::__construct(...$args);
		
		// Add templates
		$this->setMessageTemplates($this->_templates);
		
		// If array is list then it's the direct route
		if($route instanceof PluginMapSettingsInterface || ( is_array($route) && ! array_is_list($route) )):
			// Otherwise it's the routing map with settings
			$this->setSettings($route);
		else:
			$this->setRoute($this->explodeRoute($route));
		endif;
		
		// Define error controller
		$this->setErrorController();
		
		// Set current route sections
		$this->setRouteCurrent(self::DEFAULT_ROUTE_TREE);
	}
	
	/**
	 * @param    array    $route
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setRoute(array $route) : void {
		if(empty($this->_routeResolved)):
			$this->_routeResolved = $this->populateMap($route);
		else:
			throw new RuntimeException("Can't set new route because it's readonly. Please declare new instance.");
		endif;
	}
	
	/**
	 * @param    array    $route
	 * @param    array    $tree
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function populateMap(array $route, array $tree = self::DEFAULT_ROUTE_TREE) : array {
		// Check that all there is default value at least for each
		$default = array_values($tree);
		
		// Validate each route part
		foreach($default as $key => $val):
			if(! isset($route[$key])):
				// If no value defined in path then use the default
				$route[$key] = RouterDefault::fromRouterPart($val)->value;
			elseif(( $value = $this->_checkRouteMapValue($val, $route[$key]) ) !== FALSE):
				$route[$key] = $value;
			else:
				// If validation fails then use default
				$route[$key] = RouterDefault::fromRouterPart($val)->value;
			endif;
		endforeach;
		
		// This is not working because action name must be all lowercased
		#$route = array_map(function($value) { return $this->_ucfirst($value); }, $route);
		
		// Take the slice from route if there are more values than in $tree
		// If this is not checked then application can throw an error too easily. Now this can be noticed from logs.
		if(( $max = count($tree) ) !== ( $has = count($route) )):
			// Check that value amount is equal
			$route = array_slice($route, 0, ( $max ));
			
			//
			$msg = sprintf('Route has %u sections when route tree has only %u.', $has, $max);
			$this->setMessage(self::WARNING_ROUTE_TOO_MANY_SECTIONS, ['message' => $msg]);
		endif;
		
		//
		$result = array_combine(array_keys($tree), array_values($route));
		
		// Add namespace to beginning of the array.
		// Use the environment value or default value from Router default values if env doesn't exist.
		$ns = $this->_checkRouteMapValue(PlatformEnv::ENV_NAMESPACE, ( getenv(PlatformEnv::ENV_NAMESPACE->value) ??
		                                                               RouterDefault::fromRouterPart($this->_defaultPartNamespace)->value ));
		
		//
		return [...[$this->_defaultPartNamespace->value => $ns], ...$result];
	}
	
	/**
	 * @param    mixed    $key
	 * @param    mixed    $val
	 *
	 * @return false|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkRouteMapValue(mixed $key, mixed $val) : false|string {
		//
		if($key === RouterPart::VIEW || $key === RouterPart::ACTION):
			$lowercase = TRUE;
		else:
			$lowercase = FALSE;
		endif;
		
		// Value can also an object so check only scalar values
		if(is_scalar($val) && ( $this->_checkName($value = $this->_formatName($val, $lowercase), TRUE) ) !== FALSE):
			return $value;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * Check that $unformatted contains only alphanumeric character(s) and will start with alphabetic character
	 *
	 * @param    null|string    $unformatted
	 * @param    bool           $startWithAlpha
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkName(?string $unformatted = NULL, bool $startWithAlpha = FALSE) : bool {
		//
		if(isset($unformatted[0]) &&
		   ( ctype_alnum(str_replace('-', '', $unformatted)) || ctype_digit(str_replace('-', '', $unformatted)) ||
		     ctype_alnum(str_replace(' ', '', $unformatted)) )):
			
			//
			if($startWithAlpha):
				return ctype_alpha($unformatted[0]);
			else:
				return TRUE;
			endif;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @param    string    $unformatted
	 * @param    bool      $lowercase
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _formatName(string $unformatted, bool $lowercase = FALSE) : string {
		// Explode name to segment if there is a path separator
		$unformatted = trim($unformatted);
		if(! empty($unformatted) && is_array($segments = explode(PATH_SEPARATOR, $unformatted))):
			//
			foreach($segments as $key => $val):
				//
				$tmp = preg_replace('/[^a-z0-9 ]/', '', mb_strtolower($val));
				
				// Sometimes the name is required lowercase (like ACTION/VIEW)
				if($lowercase === TRUE):
					$segments[$key] = strtolower($tmp);
				else:
					$segments[$key] = ucwords($tmp);
				endif;
			endforeach;
			
			//
			return implode('_', $segments);
		endif;
		
		//
		return '';
	}
	
	/**
	 * @param    array|string    $route
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function explodeRoute(array|string $route) : array {
		if(is_string($route)):
			//
			if(str_starts_with($route, DIRECTORY_SEPARATOR) === TRUE):
				$path = explode(DIRECTORY_SEPARATOR, substr($route, 1));
			else:
				$path = explode(DIRECTORY_SEPARATOR, $route);
			endif;
		endif;
		
		//
		$this->setQuery($path ?? $route);
		
		//
		return $path ?? $route;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setErrorController() : void {
		//
		$result[RouterPart::NAMESPACE->value]  = $this->_setNamespaceNameForError();
		$result[RouterPart::CONTROLLER->value] = $this->_setControllerNameForError();
		$result[RouterPart::VIEW->value]       = $this->_setActionNameForError();
		
		//
		$this->_error = $result;
	}
	
	/**
	 * Set namespace for error controller.
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setNamespaceNameForError() : string;
	
	/**
	 * Set controller for error controller.
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setControllerNameForError() : string;
	
	/**
	 * Set action for error controller.
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setActionNameForError() : string;
	
	/**
	 * @param    array    $current
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouteCurrent(array $current) : void {
		$current[RouterPart::NAMESPACE->value]  = $this->getNamespaceName();
		$current[RouterPart::MODULE->value]     = $this->getModuleName();
		$current[RouterPart::CONTROLLER->value] = $this->getControllerName();
		$current[RouterPart::VIEW->value]       = $this->getActionName();
		
		$this->_routeCurrent = $current;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getQuery() : string {
		return $this->_query ?? '';
	}
	
	/**
	 * @param    array    $query
	 *
	 * @return AbstractMap
	 * @since   3.0.0 First time introduced.
	 */
	public function setQuery(array $query) : static {
		//
		if(empty($this->_query) && ! empty($query)):
			// Lowercase and implode
			$values = array_map(function ($value) {
				return mb_strtolower($value, $this->getEncoding());
			}, $query);
			
			// Set query string
			$this->_query = DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $values);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getErrorController() : ?string {
		if(! empty($this->_error)):
			$check = [RouterPart::NAMESPACE->value, RouterPart::CONTROLLER->value];
			foreach($check as $part):
				if(! isset($this->_error[$part])):
					$msg = sprintf('Missing %s in error controller', $part);
					throw new RuntimeException($msg);
				endif;
			endforeach;
			
			//
			$controller =
				$this->_error[RouterPart::NAMESPACE->value] . '\\' . $this->_error[RouterPart::CONTROLLER->value];
		endif;
		
		//
		return $controller ?? NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getErrorAction() : ?string {
		return $this->_error[RouterPart::VIEW->value] ?? NULL;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getDefaultRouteTree() : array {
		return $this->_defaultRouteTree;
	}
	
	/**
	 * @return AbstractMap
	 * @since   3.0.0 First time introduced.
	 */
	public function resetDefaultRouteTree() : static {
		//
		$this->_defaultRouteTree = self::DEFAULT_ROUTE_TREE;
		
		//
		return $this;
	}
	
	/**
	 * @return AbstractMap
	 * @since   3.0.0 First time introduced.
	 */
	public function updateDefaultRouteTree() : static {
		//
		$this->_defaultRouteTree = [RouterPart::MODULE->value => $this->_defaultPartModule,
		                            RouterPart::CONTROLLER->value => $this->_defaultPartController,
		                            RouterPart::VIEW->value => $this->_defaultPartAction];
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function toString() : string {
		return $this->__toString();
	}
	
	/**
	 * Will return current route in a string format with directory separator as the separator.
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function __toString() : string {
		if(! empty($this->_routeCurrent)):
			return DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $this->_routeCurrent);
		else:
			return DIRECTORY_SEPARATOR;
		endif;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRoute() : ?array {
		return $this->_routeCurrent ?? [];
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteResolved() : ?array {
		return $this->_routeResolved ?? [];
	}
	
	/**
	 * @return null|PluginMapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettings() : ?PluginMapSettingsInterface {
		return $this->_settings ?? NULL;
	}
	
	/**
	 * @param    PluginMapSettingsInterface|array    $route
	 *
	 * @return AbstractMap
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettings(PluginMapSettingsInterface|array $route) : static {
		//
		if($route instanceof PluginMapSettingsInterface):
			$this->_settings = $route;
		else:
			$this->_settings = new PluginMapSettings($route);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setNamespaceName() : string;
	
	/**
	 * @param    string    $controller
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setControllerName(string $controller) : string;
	
	/**
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setControllerBaseName(string $name) : void;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setModuleName() : string;
	
	/**
	 * @param    string    $action
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setActionName(string $action) : string;
	
	/**
	 * This is needed until PHP will release official mb_ucfirst()
	 *
	 * @param    string    $str
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _ucfirst(string $str) : string {
		return mb_strtoupper(mb_substr($str, 0, 1, $this->getEncoding()), $this->getEncoding()) .
		       mb_substr($str, 1, NULL, $this->getEncoding());
	}
}
