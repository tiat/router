<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Router;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Added the ROUTER_PARAMS_OPTIONS, ROUTER_PARAMS_DEFAULTS, ROUTER_PARAMS_ROUTE, ROUTER_PARAMS_TYPE,
 * ROUTER_PARAMS_ACTION, ROUTER_PARAMS_CONTROLLER, ROUTER_PARAMS_CONSTRAINTS constants.
 * @since   3.1.0 Added the ROUTER_PARAMS_TYPE ROUTER_PARAMS_MATCHES ROUTER_PARAMS_NAMED ROUTER_PARAMS_UNKNOWN
 * @since   3.1.1 Added the ROUTER_PARAMS_SANITIZED ROUTER_PARAMS_RAW
 */
enum RouteParams: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case DEFAULT_ROUTE_NAME_QUERY = 'query';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case DEFAULT_ROUTE_NAME_ROUTES = 'routes';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case DEFAULT_ROUTE_NAME_CHILD = 'child';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_OPTIONS = 'options';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_DEFAULTS = 'defaults';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_ROUTE = 'route';
	
	/**
	 * @since   3.1.1 First time introduced.
	 */
	case ROUTER_PARAMS_METHOD = 'method';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_CONTROLLER = 'controller';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_ACTION = 'action';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_CONSTRAINTS = 'constraints';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_TYPE = 'type';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_MATCHES = 'matches';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_NAMED = '_named';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case ROUTER_PARAMS_UNKNOWN = '_unknown';
	
	/**
	 * @since   3.1.1 First time introduced.
	 */
	case ROUTER_PARAMS_SANITIZED = '_sanitized';
	
	/**
	 * @since   3.1.1 First time introduced.
	 */
	case ROUTER_PARAMS_RAW = '_raw';
	
	/**
	 * @since   3.1.1 First time introduced.
	 */
	case ROUTER_PARAMS_KEY = 'key';
}


