<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Router;

//
use Laminas\Uri\UriFactory;
use Psr\Http\Message\RequestInterface;
use Tiat\Router\Exception\RuntimeException;
use Tiat\Router\Router\Map;
use Tiat\Router\Router\Plugin\PluginMapInterface;
use Tiat\Router\Router\RouteParams;
use Tiat\Standard\DataModel\HttpMethod;
use Tiat\Standard\Loader\ClassLoaderInterface;

use function array_shift;
use function array_slice;
use function class_exists;
use function constant;
use function count;
use function defined;
use function explode;
use function is_array;
use function sprintf;
use function str_ends_with;
use function str_starts_with;
use function substr;
use function trigger_error;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Router extends AbstractRouter {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string APPLICATION_ROUTE_MAP = 'ApplicationConfig\RouteMap';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string APPLICATION_ROUTE_MAP_INTERFACE = PluginMapInterface::class;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const int LANGUAGE_ELEMENT_INDEX_AFTER = 0;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const int LANGUAGE_ELEMENT_INDEX_BEFORE = 0;
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = FALSE, bool $autorun = FALSE, ...$args) {
		//
		parent::__construct($params, $autoinit, FALSE, ...$args);
		
		//
		$this->_defineAutostart($autoinit, $autorun, ...$args);
	}
	
	/**
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : ClassLoaderInterface {
		// Resolve application route map (if exists)
		if($this->isInitCompleted() === FALSE):
			return parent::init($args);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		//
		if(( $request = $this->getRequest() ) === NULL):
			throw new RuntimeException("Request interface doesn't exist and should be fetch from MessageBus. This is development error.");
		endif;
		
		// Request interface exists
		if($request instanceof RequestInterface):
			$this->setParam('url', (string)$request->getUri());
		else:
			$msg = sprintf("Request is not instance of %s.", RequestInterface::class);
			throw new RuntimeException($msg);
		endif;
		
		//
		if($this->isInitCompleted() === TRUE):
			// Detect language and queried route
			$this->detectLanguage();
			
			// Route maps should be resolved here
			if(( $map = $this->getRouteMap(RouteParams::DEFAULT_ROUTE_NAME_QUERY) ) !== NULL):
				// Add METHOD to route map
				$map->setRouteMethod(HttpMethod::tryFrom($request->getMethod()));
				
				// Send map(s) forward by router type (tree/simple/custom)
				if(( $router = $this->getRouterPlugin() ) !== NULL):
					// Add default query based map
					$router->addRouteDefault($map);
					
					// Resolve child (map have checked the modules etc.)
					#$this->_resolveChildRoute($map->getModuleName(), (string)$map->getControllerBaseName());
					
					// Add child routers if any (not needed as default)
					if(( $child = $this->getRouteMap(RouteParams::DEFAULT_ROUTE_NAME_ROUTES) ) !== NULL):
						if(is_array($child)):
							$router->addRouteChilds($child);
						else:
							// If child routes are not in array format then use this as default fallback
							$router->addRouteChild(RouteParams::DEFAULT_ROUTE_NAME_ROUTES, $child);
						endif;
					endif;
					
					// Match the route
					$router->matchRoute();
					
					//
					$args['hasRun'] = TRUE;
					parent::run(...$args);
				endif;
			else:
				throw new RuntimeException("Router doesn't have any map to route.");
			endif;
			
			//
			$this->setRun(TRUE);
		endif;
		
		return $this;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function detectLanguage() : void {
		// As default, language is inside the path.
		if($this->getLanguageSetting(self::DEFAULT_PATH_KEY_PATH) === self::DEFAULT_PATH_AFTER):
			$this->addRouteMap(RouteParams::DEFAULT_ROUTE_NAME_QUERY, $map = new Map($this->_detectLanguageAfter()));
			$map->setLanguage($this->getLanguage());
		elseif($this->getLanguageSetting(self::DEFAULT_PATH_KEY_PATH) === self::DEFAULT_PATH_BEFORE):
			// Lang is in front of the path, inside domain name.
			$this->_detectLanguageBefore();
			trigger_error(__METHOD__ . ' : Map instance is not set, check is this dev error.', E_USER_WARNING);
		else:
			// No language detection path specified. Specially with API requests, we don't need to detect language.
			$this->addRouteMap(RouteParams::DEFAULT_ROUTE_NAME_QUERY, new Map($this->_getRouteWithoutLanguage()));
		endif;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _detectLanguageAfter() : array {
		// Get the default path
		$path = $this->_getRouteWithoutLanguage();
		
		// Try fetch the language from url path
		if(! empty($path)):
			//
			$this->setLanguage($path[self::LANGUAGE_ELEMENT_INDEX_AFTER], self::DEFAULT_LANGUAGE);
			
			// Remove the first element from array & generate new route path.
			// Don't do anything if index is not at the beginning.
			if(self::LANGUAGE_ELEMENT_INDEX_AFTER === 0):
				array_shift($path);
			endif;
			
			//
			return $path;
		endif;
		
		//
		return [];
	}
	
	/**
	 * Get the base route path without language and exploded to array.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getRouteWithoutLanguage() : array {
		$uri = UriFactory::factory($this->getParam('url'));
		if(! empty($p = $uri->getPath())):
			//
			if(str_starts_with($p, DIRECTORY_SEPARATOR) === TRUE):
				$path = explode(DIRECTORY_SEPARATOR, substr($p, 1));
			else:
				$path = explode(DIRECTORY_SEPARATOR, $p);
			endif;
			
			//
			return $path;
		endif;
		
		//
		return [];
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _detectLanguageBefore() : void {
		//
		$server = ( UriFactory::factory($this->getParam('url')) )->getHost();
		if(str_ends_with($server, 'localhost') === TRUE):
			if(count($e = explode('.', $server)) > 1):
				$e = array_slice($e, 0, count($e) - 1);
			else:
				return;
			endif;
		else:
			$e = explode('.', $server);
		endif;
		
		//
		if(defined(self::URL_SECTIONS_PATTERN) && ! empty($pattern = constant(self::URL_SECTIONS_PATTERN)) &&
		   ! empty($values = explode('.', $pattern))):
			//
			foreach($values as $key => $value):
				if($value === '{' . self::URL_SECTIONS_LANGUAGE . '}'):
					$index = $key;
					break;
				endif;
			endforeach;
		endif;
		
		//
		if(! empty($e)):
			$lang = $e[$index ?? self::LANGUAGE_ELEMENT_INDEX_BEFORE];
			$this->setLanguage($lang, self::DEFAULT_LANGUAGE);
		else:
			$msg = sprintf("Constant %s is not defined.", self::URL_SECTIONS_PATTERN);
			throw new RuntimeException($msg);
		endif;
	}
	
	/**
	 * @param    string    $module
	 * @param    string    $controller
	 *
	 * @return void
	 * @TODO Finish this method
	 */
	protected function _resolveChildRoute(string $module, string $controller) : void {
		//
		$class = self::APPLICATION_ROUTE_MAP;
		if(class_exists($class)):
			//
			$client   = new $class();
			$contract = self::APPLICATION_ROUTE_MAP_INTERFACE;
			if($client instanceof $contract):
				$this->addRouteMap(RouteParams::DEFAULT_ROUTE_NAME_CHILD, new Map($client->getRouteMap($module) ?? []));
			endif;
		endif;
	}
}
