<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Define router model (MVC, MVC, MVVM, VIEW, CUSTOM).
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum RouterModel: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * Use this if you have your own module which doesn't fit to others on this list
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CUSTOM = 'CUSTOM';
	
	/**
	 * Usually the default pattern
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case MVC = 'MVC';
	
	/**
	 * Use this if you have extra requirements
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case MVP = 'MVP';
	
	/**
	 * Use this if you have extra requirements
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case MVVM = 'MVVM';
	
	/**
	 * REST services
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case REST = 'REST';
	
	/**
	 * Most simplified model
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case VIEW = 'VIEW';
}
