<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Define router type.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum RouterType: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * User custom router type.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CUSTOM = 'CUSTOM';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SIMPLE = 'SIMPLE';
	
	/**
	 * Default value for all.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case TREE = 'TREE';
}
