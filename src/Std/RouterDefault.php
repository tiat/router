<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Std;

//
use Tiat\Router\Exception\InvalidArgumentException;
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

use function get_debug_type;
use function sprintf;

/**
 * Define router part defaults
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum RouterDefault: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * This will bypass the default method names with user own. Use this carefully.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CUSTOM = 'custom';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case NAMESPACE = 'default';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CONTROLLER = 'index';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case FALLBACK = 'fallback';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ERROR = 'error';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterDefault MODULE = self::NAMESPACE;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterDefault VIEW = self::CONTROLLER;
	
	/**
	 * @param    RouterPart    $part
	 *
	 * @return RouterDefault
	 * @since   3.0.0 First time introduced.
	 */
	public static function fromRouterPart(RouterPart $part) : RouterDefault {
		return match ( $part ) {
			RouterPart::NAMESPACE => self::NAMESPACE,
			RouterPart::MODULE => self::MODULE,
			RouterPart::CONTROLLER => self::CONTROLLER,
			RouterPart::VIEW, RouterPart::ACTION => self::VIEW,
			RouterPart::ERROR => self::ERROR,
			RouterPart::FALLBACK => self::FALLBACK,
			default => throw new InvalidArgumentException(sprintf("Given %s doesn't have default match.",
			                                                      get_debug_type($part)))
		};
	}
}
