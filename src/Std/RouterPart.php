<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Define router parts
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum RouterPart: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CONTROLLER = 'CONTROLLER';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CUSTOM = 'CUSTOM';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case DEFAULT = 'DEFAULT';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ERROR = 'ERROR';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case FALLBACK = 'FALLBACK';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case INDEX = 'INDEX';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case MODULE = 'MODULE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case NAMESPACE = 'NAMESPACE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case QUERY = 'QUERY';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case VIEW = 'ACTION';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const RouterPart ACTION = self::VIEW;
}
