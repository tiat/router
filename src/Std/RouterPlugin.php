<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Define router plugin types.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum RouterPlugin: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case HTTP = 'HTTP';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case HTTP_METHOD = 'HTTP_METHOD';
	
	/**
	 * Use this if you want to write your own route configuration files.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case LITERAL = 'LITERAL';
}
