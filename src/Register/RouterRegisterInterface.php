<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Router\Register;

//
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Standard\Register\RegisterPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface RouterRegisterInterface extends RegisterInterface, RegisterPluginInterface {

}
