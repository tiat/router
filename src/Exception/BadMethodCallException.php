<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Exception;

/**
 * @version 3.0.0
 * @since   3.1.1 First time introduced.
 */
class BadMethodCallException extends \BadMethodCallException implements ExceptionInterface {

}
