<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Request;

//
use Laminas\Diactoros\Uri;
use Psr\Http\Message\UriInterface;
use Tiat\Standard\Request\RequestInterface;
use Tiat\Stdlib\Exception\InvalidArgumentException;
use Tiat\Stdlib\Message\MessageTrait;

use function array_keys;
use function is_string;
use function preg_match;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait RequestTrait {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use MessageTrait;
	
	/**
	 * @var UriInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected UriInterface $_uri;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_method = 'GET';
	
	/**
	 * @var null|string
	 * @since   3.0.0 First time introduced.
	 */
	private ?string $_requestTarget;
	
	/**
	 * @param    null|UriInterface|string    $uri
	 * @param    null|string                 $method
	 * @param    string                      $body
	 * @param    array                       $headers
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function initialize(UriInterface|string|null $uri = NULL, ?string $method = NULL, string $body = 'php://memory', array $headers = []) : void {
		//
		if($method !== NULL):
			$this->setMethod($method);
		endif;
		
		//
		$this->_uri    = $this->_createUri($uri);
		$this->_stream = $this->_getStream($body, 'wb+');
		
		//
		$this->_setHeaders($headers);
		
		// PSR-7: attempt to set the Host header from a provided URI if no
		// Host header is provided
		if(! $this->hasHeader('Host') && $this->_uri->getHost()):
			$this->_headerNames['host'] = 'Host';
			$this->_headers['Host']     = [$this->_getHostFromUri()];
		endif;
	}
	
	/**
	 * @param    null|string|UriInterface    $uri
	 *
	 * @return UriInterface
	 * @since   3.0.0 First time introduced.
	 */
	private function _createUri(null|string|UriInterface $uri) : UriInterface {
		//
		if($uri instanceof UriInterface):
			return $uri;
		endif;
		
		//
		if(is_string($uri)):
			return new Uri($uri);
		endif;
		
		//
		return new Uri();
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getHostFromUri() : string {
		//
		$host = $this->_uri->getHost();
		$host .= $this->_uri->getPort() !== NULL ? ':' . $this->_uri->getPort() : '';
		
		//
		return $host;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestTarget() : string {
		//
		if($this->_requestTarget === NULL):
			return $this->_requestTarget;
		endif;
		
		//
		$target = $this->_uri->getPath();
		if($this->_uri->getQuery()):
			$target .= '?' . $this->_uri->getQuery();
		endif;
		
		//
		if(empty($target)):
			$target = '/';
		endif;
		
		//
		return $target;
	}
	
	/**
	 * @param    mixed    $requestTarget
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function withRequestTarget(mixed $requestTarget) : RequestInterface {
		//
		if(preg_match('#\s#', $requestTarget)):
			throw new InvalidArgumentException('Invalid request target provided; cannot contain whitespace');
		endif;
		
		//
		$new                 = clone $this;
		$new->_requestTarget = $requestTarget;
		
		//
		return $new;
	}
	
	/**
	 * @return string Returns the request method.
	 * @since   3.0.0 First time introduced.
	 */
	public function getMethod() : string {
		return $this->_method;
	}
	
	/**
	 * @param    string    $method
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMethod(string $method) : RequestInterface {
		//
		if(! preg_match('/^[!#$%&\'*+.^_`|~0-9a-z-]+$/i', $method)):
			$msg = sprintf('Unsupported HTTP method "%s" provided', $method);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		$this->_method = $method;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $originals
	 *
	 * @return RequestInterface
	 * @since   3.1.1 First time introduced.
	 */
	public function setHeaders(array $originals) : RequestInterface {
		//
		$this->_setHeaders($originals);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $method    Case-sensitive method.
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function withMethod(string $method) : RequestInterface {
		//
		$new = clone $this;
		$new->setMethod($method);
		
		//
		return $new;
	}
	
	/**
	 * @return UriInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getUri() : UriInterface {
		return $this->_uri;
	}
	
	/**
	 * @param    UriInterface    $uri
	 * @param    bool            $preserveHost
	 *
	 * @return RequestInterface
	 * @since   3.1.1 First time introduced.
	 */
	public function setUri(UriInterface $uri, bool $preserveHost = FALSE) : RequestInterface {
		return $this->withUri($uri, $preserveHost);
	}
	
	/**
	 * @param    UriInterface    $uri             New request URI to use.
	 * @param    bool            $preserveHost    Preserve the original state of the Host header.
	 *
	 * @return RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function withUri(UriInterface $uri, bool $preserveHost = FALSE) : RequestInterface {
		//
		$new       = clone $this;
		$new->_uri = $uri;
		
		//
		if($preserveHost && $this->hasHeader('Host')):
			return $new;
		endif;
		
		//
		if(! $uri->getHost()):
			return $new;
		endif;
		
		//
		$host = $uri->getHost();
		if($uri->getPort()):
			$host .= ':' . $uri->getPort();
		endif;
		
		//
		$new->_headerNames['host'] = 'Host';
		
		// Remove an existing host header if present, regardless of current
		// de-normalization of the header name
		foreach(array_keys($new->_headers) as $header):
			if(strtolower($header) === 'host'):
				unset($new->_headers[$header]);
			endif;
		endforeach;
		
		//
		$new->_headers['Host'] = [$host];
		
		//
		return $new;
	}
}
