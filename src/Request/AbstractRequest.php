<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Request;

//
use Tiat\Standard\Request\RequestInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractRequest implements RequestInterface, RouterRequestTraitInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use RequestTrait;
}
