<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Router\Request;

//
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface RouterRequestTraitInterface extends RequestInterface {
	
	/**
	 * @param    null|UriInterface|string    $uri
	 * @param    null|string                 $method
	 * @param    string                      $body
	 * @param    array                       $headers
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function initialize(UriInterface|string|null $uri = NULL, ?string $method = NULL, string $body = 'php://memory', array $headers = []) : void;
}