<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Router\Request;

//
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

/**
 * PSR-7
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Request extends AbstractRequest {
	
	/**
	 * @param    NULL|string|UriInterface    $uri
	 * @param    null|string                 $method
	 * @param    StreamInterface|string      $body
	 * @param    array                       $headers
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(UriInterface|string|null $uri = NULL, ?string $method = NULL, StreamInterface|string $body = 'php://temp', array $headers = []) {
		$this->initialize($uri, $method, $body, $headers);
	}
}
