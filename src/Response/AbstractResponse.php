<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response;

//
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use Tiat\Router\Exception\InvalidArgumentException;
use Tiat\Stdlib\Message\MessageTrait;
use Tiat\Stdlib\Response\ResponseStatus;

use function is_numeric;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractResponse implements MessageInterface, ResponseInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use MessageTrait;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_statusCode;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_statusPhrase;
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getStatusCode() : int {
		return $this->_statusCode;
	}
	
	/**
	 * @param    int       $code
	 * @param    string    $reasonPhrase
	 *
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setStatusCode(int $code, string $reasonPhrase = '') : void {
		//
		if($code < ResponseStatus::MIN_STATUS || $code > ResponseStatus::MAX_STATUS):
			//
			$msg = sprintf('Code must be between %u and %u, received %d', ResponseStatus::MIN_STATUS,
			               ResponseStatus::MAX_STATUS, $code);
			
			//
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		if(empty($reasonPhrase) && ! is_numeric($e = ResponseStatus::getResponse($code))):
			$this->_statusPhrase = $e;
		else:
			$this->_statusPhrase = $reasonPhrase;
		endif;
		
		//
		$this->_statusCode = $code;
	}
	
	/**
	 * @param    int       $code
	 * @param    string    $reasonPhrase
	 *
	 * @return Response
	 * @since   3.0.0 First time introduced.
	 */
	public function withStatus(int $code, string $reasonPhrase = '') : static {
		//
		$new = clone $this;
		$new->_setStatusCode(code:$code, reasonPhrase:$reasonPhrase);
		
		//
		return $new;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getReasonPhrase() : string {
		return $this->_statusPhrase;
	}
}
