<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum ResponseFormat: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case CSV = 'csv';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case EMPTY = 'empty';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case HTML = 'html';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case JSON = 'json';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case REDIRECT = 'redirect';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case RSS = 'rss';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TEXT = 'text';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case XML = 'xml';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case YAML = 'yaml';
}
