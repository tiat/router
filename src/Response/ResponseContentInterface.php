<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ResponseContentInterface {
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseCode() : ?int;
	
	/**
	 * @param    int    $code
	 *
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setResponseCode(int $code) : ResponseContentInterface;
	
	/**
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetResponseCode() : ResponseContentInterface;
	
	/**
	 * @return null|ResponseFormat
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseFormat() : ?ResponseFormat;
	
	/**
	 * @param    ResponseFormat    $responseFormat
	 *
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setResponseFormat(ResponseFormat $responseFormat) : ResponseContentInterface;
	
	/**
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetResponseFormat() : ResponseContentInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseContent() : mixed;
	
	/**
	 * @param    mixed             $content
	 * @param    ResponseFormat    $responseFormat
	 * @param    int               $code
	 *
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setResponseContent(mixed $content, ResponseFormat $responseFormat, int $code) : ResponseContentInterface;
	
	/**
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetResponseContent() : ResponseContentInterface;
}
