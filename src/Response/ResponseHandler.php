<?php

/**
 * Tiat Framework
 *
 * @package        Tiat\Router
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Router\Response;

//
use Psr\Http\Message\StreamInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class ResponseHandler extends Response {
	
	/**
	 * @param    StreamInterface|string    $body
	 * @param    int                       $status
	 * @param    array                     $headers
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(StreamInterface|string $body = 'php://memory', int $status = 200, array $headers = []) {
		//
		$this->_setStatusCode($status);
		
		//
		$this->_stream = $this->_getStream($body, 'wb+');
		
		//
		$this->_setHeaders($headers);
	}
}
