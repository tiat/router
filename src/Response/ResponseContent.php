<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response;

//
use Tiat\Stdlib\Response\ResponseStatus;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait ResponseContent {
	
	/**
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	private mixed $_responseContent;
	
	/**
	 * JSON, XML, HTML etc
	 *
	 * @var ResponseFormat
	 * @see     ResponseFormat
	 * @since   3.0.0 First time introduced.
	 */
	private ResponseFormat $_responseFormat;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_responseCode;
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseCode() : ?int {
		return $this->_responseCode ?? NULL;
	}
	
	/**
	 * @param    int    $code
	 *
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setResponseCode(int $code) : ResponseContentInterface {
		//
		if(! empty(ResponseStatus::checkStatusCode($code))):
			$this->_setResponseCode($code);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetResponseCode() : ResponseContentInterface {
		//
		unset($this->_responseCode);
		
		//
		return $this;
	}
	
	/**
	 * @return null|ResponseFormat
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseFormat() : ?ResponseFormat {
		return $this->_responseFormat ?? NULL;
	}
	
	/**
	 * @param    ResponseFormat    $responseFormat
	 *
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setResponseFormat(ResponseFormat $responseFormat) : ResponseContentInterface {
		//
		$this->_responseFormat = $responseFormat;
		
		//
		return $this;
	}
	
	/**
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetResponseFormat() : ResponseContentInterface {
		//
		unset($this->_responseFormat);
		
		//
		return $this;
	}
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseContent() : mixed {
		return $this->_responseContent ?? NULL;
	}
	
	/**
	 * @param    mixed                  $content
	 * @param    null|ResponseFormat    $responseFormat
	 * @param    null|int               $code
	 *
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setResponseContent(mixed $content, ?ResponseFormat $responseFormat = NULL, ?int $code = NULL) : ResponseContentInterface {
		//
		$this->_responseContent = $content;
		
		//
		if($responseFormat !== NULL):
			$this->setResponseFormat($responseFormat);
		endif;
		
		//
		if($code !== NULL):
			$this->setResponseCode($code);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetResponseContent() : ResponseContentInterface {
		//
		unset($this->_responseContent);
		
		//
		return $this;
	}
	
	/**
	 * @param    int    $code
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _setResponseCode(int $code) : void {
		// Delete variable from ParametersPlugin if exists
		// This is a workaround for PHP internal error when $this->_responseCode is accessed properly.
		// This is some kind of bug in PHP.
		if(method_exists($this, 'hasParam') && $this->hasParam('_responseCode') === TRUE):
			$this->deleteParam('_responseCode');
		endif;
		
		$this->_responseCode = $code;
	}
}
