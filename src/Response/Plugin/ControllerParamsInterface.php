<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Plugin;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Fixed namespace.
 */
interface ControllerParamsInterface {
	
	/**
	 * Get single query param with a name from all possible params (including GET/POST/ENV/SERVER/COOKIE & headers)
	 * or null to get all
	 *
	 * @param    NULL|string    $name        Parameter name to retrieve, or null to get all
	 * @param    null|mixed     $default     Default value to use when the requested arg is missing
	 * @param    bool           $wildcard    Search arg with wildcard (at least one character is needed in name)
	 *                                       Arg must contain the name
	 *
	 * @return mixed
	 */
	public function getAll(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Return all query parameters or a single query parameter.
	 * Request from (HTTP) headers
	 *
	 * @param    NULL|string    $name        Parameter name to retrieve or null to get all
	 * @param    null|mixed     $default     Default value to use when the requested arg is missing
	 * @param    bool           $wildcard    Search arg with wildcard (at least one character is needed in name)
	 *                                       Arg must contain the name
	 *
	 * @return mixed
	 */
	public function fromHeader(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Return all query parameters or a single query parameter.
	 * Same as $_POST
	 *
	 * @param    NULL|string    $name        Parameter name to retrieve or null to get all
	 * @param    null|mixed     $default     Default value to use when the requested arg is missing
	 * @param    bool           $wildcard    Search arg with wildcard (at least one character is needed in name)
	 *                                       Arg must contain the name
	 *
	 * @return mixed
	 */
	public function fromPost(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Return all query parameters or a single query parameter.
	 * Same as $_GET
	 *
	 * @param    NULL|string    $name        Parameter name to retrieve or null to get all
	 * @param    null|mixed     $default     Default value to use when the requested arg is missing
	 * @param    bool           $wildcard    Search arg with wildcard (at least one character is needed in name)
	 *                                       Arg must contain the name
	 *
	 * @return mixed
	 */
	public function fromQuery(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Return all query parameters or a single query parameter.
	 * Same as $_COOKIE
	 *
	 * @param    NULL|string    $name        Parameter name to retrieve or null to get all
	 * @param    null|mixed     $default     Default value to use when the requested arg is missing
	 * @param    bool           $wildcard    Search arg with wildcard (at least one character is needed in name)
	 *                                       Arg must contain the name
	 *
	 * @return mixed
	 */
	public function fromCookie(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Return all files or a single file
	 *
	 * @param    NULL|string    $name        File name to retrieve or null to get all
	 * @param    null|mixed     $default     Default value to use when the requested arg is missing
	 * @param    bool           $wildcard    Search arg with wildcard (at least one character is needed in name)
	 *                                       Arg must contain the name
	 *
	 * @return mixed
	 */
	public function fromFiles(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Set params for search
	 *
	 * @param    array    $params
	 *
	 * @return ControllerParamsInterface
	 */
	public function defineRegistry(array $params) : ControllerParamsInterface;
	
	/**
	 * If searching with name then letter case matters. With this you can search params without taking care about that.
	 * Default is FALSE which means that search is NOT case-sensitive
	 *
	 * @param    bool    $value
	 *
	 * @return ControllerParamsInterface
	 */
	public function setCaseSensitive(bool $value) : ControllerParamsInterface;
}
