<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Plugin;

//
use WeakMap;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Fixed namespace.
 */
interface ControllerPluginInterface {
	
	/**
	 * @param    array    $params
	 *
	 * @return ControllerPluginInterface
	 */
	public function setParamsRegistry(array $params) : ControllerPluginInterface;
	
	/**
	 * @return ControllerParamsInterface
	 */
	public function getParams() : ControllerParamsInterface;
	
	/**
	 * @param    WeakMap    $registry
	 *
	 * @return ControllerPluginInterface
	 */
	public function setUrlRegistry(WeakMap $registry) : ControllerPluginInterface;
	
	/**
	 * @return ControllerUrlInterface
	 */
	public function getUrl() : ControllerUrlInterface;
}
