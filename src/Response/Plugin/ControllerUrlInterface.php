<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Plugin;

//
use Tiat\Router\Std\RouterPart;
use WeakMap;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Fixed namespace.
 */
interface ControllerUrlInterface {
	
	/**
	 * Get
	 *
	 * @param    RouterPart    $name    Return whole path if null, otherwise named part
	 *
	 * @return string
	 */
	public function fromQuery(RouterPart $name) : string;
	
	/**
	 * @param    RouterPart    $name
	 *
	 * @return string
	 */
	public function fromResolved(RouterPart $name) : string;
	
	/**
	 * @param    string    $method
	 *
	 * @return ControllerUrlInterface
	 */
	public function setMethod(string $method) : ControllerUrlInterface;
	
	/**
	 * @return null|string
	 */
	public function getMethod() : ?string;
	
	/**
	 * @param    WeakMap    $registry
	 *
	 * @return ControllerUrlInterface
	 */
	public function defineRegistry(WeakMap $registry) : ControllerUrlInterface;
}
