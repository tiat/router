<?php

/**
 * Tiat Framework
 *
 * @package        Tiat\Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Format;

//
use function array_keys;
use function array_reduce;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait InjectContentType {
	
	/**
	 * @param    string    $contentType
	 * @param    array     $headers
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function injectContentType(string $contentType, array $headers) : array {
		//
		$hasContentType = array_reduce(array_keys($headers), static function ($carry, $item) {
			return $carry ?: ( strtolower($item) === 'content-type' );
		},                             FALSE);
		
		//
		if(! $hasContentType):
			$headers['content-type'] = [$contentType];
		endif;
		
		//
		return $headers;
	}
}
