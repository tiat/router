<?php

/**
 * Tiat Framework
 *
 * @package        Tiat\Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Format;

//
use JsonException;
use Laminas\Diactoros\Stream;
use Tiat\Router\Response\ResponseHandler;
use Tiat\Stdlib\Exception\InvalidArgumentException;

use function is_resource;
use function json_encode;
use function json_last_error;
use function json_last_error_msg;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class JsonResponse extends ResponseHandler {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use InjectContentType;
	
	/**
	 * JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const int DEFAULT_JSON_FLAGS = 79;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_encodingOptions;
	
	/**
	 * @param    mixed    $data
	 * @param    int      $responseCode
	 * @param    array    $headers
	 * @param    mixed    ...$args
	 *
	 * @throws JsonException
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(mixed $data, int $responseCode = 200, array $headers = [], ...$args) {
		
		//
		$this->_encodingOptions = (int)( $args[0] ?? self::DEFAULT_JSON_FLAGS );
		
		//
		$json = $this->_setData($data, $this->_encodingOptions);
		$body = $this->_createBody($json);
		
		//
		$headers = $this->injectContentType('application/json', $headers);
		
		//
		parent::__construct($body, $responseCode, $headers);
	}
	
	/**
	 * @param    mixed    $data
	 * @param    int      $options
	 *
	 * @return string
	 * @throws JsonException
	 * @since   3.0.0 First time introduced.
	 */
	private function _setData(mixed $data, int $options) : string {
		//
		if(is_resource($data)):
			throw new InvalidArgumentException('Cannot JSON encode resources');
		endif;
		
		//
		$json = json_encode($data, JSON_THROW_ON_ERROR | $options);
		
		//
		if(json_last_error() !== JSON_ERROR_NONE):
			$msg = sprintf('Unable to encode data to JSON in %s: %s', __CLASS__, json_last_error_msg());
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $json;
	}
	
	/**
	 * @param    string    $json
	 *
	 * @return Stream
	 * @since   3.0.0 First time introduced.
	 */
	private function _createBody(string $json) : Stream {
		//
		$body = new Stream('php://temp', 'wb+');
		$body->write($json);
		$body->rewind();
		
		//
		return $body;
	}
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncodingOptions() : int {
		return $this->_encodingOptions ?? 0;
	}
}
