<?php

/**
 * Tiat Framework
 *
 * @package        Tiat\Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Format;

//
use Laminas\Diactoros\Stream;
use Psr\Http\Message\StreamInterface;
use Tiat\Router\Response\ResponseHandler;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class XmlResponse extends ResponseHandler {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use InjectContentType;
	
	/**
	 * @param    string|StreamInterface    $body
	 * @param    int                       $status
	 * @param    array                     $headers
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(string|StreamInterface $body = 'php://memory', int $status = 200, array $headers = []) {
		parent::__construct($this->createBody($body), $status,
		                    $this->injectContentType('application/xml; charset=utf-8', $headers));
	}
	
	/**
	 * @param    string|StreamInterface    $xml
	 *
	 * @return StreamInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function createBody(string|StreamInterface $xml) : StreamInterface {
		if($xml instanceof StreamInterface):
			return $xml;
		endif;
		
		//
		$body = new Stream('php://temp', 'wb+');
		$body->write($xml);
		$body->rewind();
		
		//
		return $body;
	}
}
