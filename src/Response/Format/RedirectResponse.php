<?php

/**
 * Tiat Framework
 *
 * @package        Tiat\Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Format;

//
use Psr\Http\Message\UriInterface;
use Tiat\Router\Response\ResponseHandler;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class RedirectResponse extends ResponseHandler {
	
	/**
	 * @param    UriInterface|string    $uri
	 * @param    int                    $status
	 * @param    array                  $headers
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(UriInterface|string $uri, int $status = 302, array $headers = []) {
		//
		$headers['location'] = [(string)$uri];
		parent::__construct('php://temp', $status, $headers);
	}
}
