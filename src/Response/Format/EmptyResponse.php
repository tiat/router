<?php

/**
 * Tiat Framework
 *
 * @package        Tiat\Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Format;

//
use Laminas\Diactoros\Stream;
use Tiat\Router\Response\ResponseHandler;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EmptyResponse extends ResponseHandler {
	
	/**
	 * @param    int      $status
	 * @param    array    $headers
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(int $status = 204, array $headers = []) {
		//
		$body = new Stream('php://temp', 'r');
		
		//
		parent::__construct($body, $status, $headers);
	}
}
