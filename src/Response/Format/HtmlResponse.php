<?php

/**
 * Tiat Framework
 *
 * @package        Tiat\Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Router\Response\Format;

//
use Laminas\Diactoros\Stream;
use Psr\Http\Message\StreamInterface;
use Tiat\Router\Response\ResponseHandler;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class HtmlResponse extends ResponseHandler {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use InjectContentType;
	
	/**
	 * @param    string|StreamInterface    $body
	 * @param    int                       $status
	 * @param    array                     $headers
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(string|StreamInterface $body = 'php://memory', int $status = 200, array $headers = []) {
		parent::__construct($this->_createBody($body), $status,
		                    $this->injectContentType('text/html; charset=utf-8', $headers));
	}
	
	/**
	 * @param    StreamInterface|string    $html
	 *
	 * @return StreamInterface
	 * @since   3.0.0 First time introduced.
	 */
	private function _createBody(StreamInterface|string $html) : StreamInterface {
		//
		if($html instanceof StreamInterface):
			return $html;
		endif;
		
		//
		$body = new Stream('php://temp', 'wb+');
		$body->write($html);
		$body->rewind();
		
		//
		return $body;
	}
}
