<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Tiat\Router;

//
use Jantia\Asi\Register\AsiRegisterParams;
use Jantia\Stdlib\Message\Message;
use Psr\Http\Message\RequestInterface;
use Tiat\Router\Exception\InvalidArgumentException;
use Tiat\Router\Register\RouterRegister;
use Tiat\Router\Router\MapInterface;
use Tiat\Router\Router\RouteParams;
use Tiat\Router\Router\Type\RouterTypeInterface;
use Tiat\Router\Router\Type\RouterTypeTree;
use Tiat\Router\Std\RouterModel;
use Tiat\Router\Std\RouterType;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Router\RouterInterface;
use Tiat\Stdlib\Loader\ClassLoaderTrait;
use Tiat\Stdlib\Parameters\ParametersPlugin;
use Tiat\Stdlib\Register\RegisterContainerInterface;

use function array_keys;
use function implode;
use function in_array;
use function is_string;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractRouter extends Message implements ClassLoaderInterface, ParametersPluginInterface, RouterInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait;
	use ParametersPlugin;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_PATH_AFTER = 'after';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_PATH_BEFORE = 'before';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const array DEFAULT_PATH_SETTINGS = [self::DEFAULT_PATH_AFTER, self::DEFAULT_PATH_BEFORE];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_LANGUAGE = 'en';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_PATH_KEY_PATH = 'path';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_PATH_KEY_DEFAULT = 'default';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const array DEFAULT_LANGUAGE_SETTINGS = [self::DEFAULT_PATH_KEY_PATH => NULL,
	                                                self::DEFAULT_PATH_KEY_DEFAULT => self::DEFAULT_LANGUAGE];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_ROUTER_REGISTRY = RouterRegister::class;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const RouterModel DEFAULT_ROUTER_MODEL = RouterModel::MVC;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const RouterType DEFAULT_ROUTER_TYPE = RouterType::TREE;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const array DEFAULT_ROUTER_TYPE_LIST = [RouterType::TREE->value => RouterTypeTree::class];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	#public const string DEFAULT_ROUTE_NAME_QUERY = 'query';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	#public const string DEFAULT_ROUTE_NAME_ROUTES = 'routes';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	#public const string DEFAULT_ROUTE_NAME_CHILD = 'child';
	
	/**
	 * @var RouterModel
	 * @since   3.0.0 First time introduced.
	 */
	private RouterModel $_routerModel = self::DEFAULT_ROUTER_MODEL;
	
	/**
	 * @var RouterType
	 * @since   3.0.0 First time introduced.
	 */
	private RouterType $_routerType = self::DEFAULT_ROUTER_TYPE;
	
	/**
	 * @var RequestInterface
	 */
	private RequestInterface $_requestInterface;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_language = [self::DEFAULT_LANGUAGE];
	
	/**
	 * Language settings
	 *
	 * @var null|array
	 * @since   3.0.0 First time introduced.
	 */
	private ?array $_languageSettings = self::DEFAULT_LANGUAGE_SETTINGS;
	
	/**
	 * @var array|string
	 * @since   3.0.0 First time introduced.
	 */
	private array|string $_route;
	
	/**
	 * @var array
	 * @var array
	 */
	private array $_routeMap;
	
	/**
	 * @var RouterTypeInterface
	 */
	private RouterTypeInterface $_routerTypePlugin;
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 */
	public function __construct(iterable $params = [], bool $autoinit = FALSE, bool $autorun = FALSE, ...$args) {
		//
		parent::__construct(...$args);
		
		//
		$this->_defineAutostart($autoinit, $autorun, ...$args);
		
		//
		if(! empty($params)):
			$this->setParams($params);
		endif;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 */
	public function __invoke(...$args) : static {
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 */
	public function init(...$args) : ClassLoaderInterface {
		//
		$this->setInitCompleted(TRUE);
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void {
	
	}
	
	/**
	 * @return RouterModel
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouterModel() : RouterModel {
		return $this->_routerModel;
	}
	
	/**
	 * @param    array    $routerModel
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouterModel(array $routerModel) : RouterInterface {
		// Get router type
		$this->_resolveRouterType($routerModel);
		if(empty($handler = $routerModel[AsiRegisterParams::HANDLER->value] ?? NULL)):
			$handler = $this->_getDefaultRouterType($this->getRouterType());
		endif;
		
		//
		$params = $routerModel[AsiRegisterParams::PARAMS->value] ?? NULL;
		
		//
		$r = new $handler($params);
		if(( ! empty($contract = $routerModel[AsiRegisterParams::CONTRACT->value] ?? NULL) &&
		     ! $r instanceof $contract )):
			$msg = sprintf("Handler %s is not a instance of %s.", $handler, $contract);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		if(! empty($settings = $routerModel[AsiRegisterParams::SETTINGS->value] ?? [])):
			if($settings instanceof RegisterContainerInterface):
				$this->setSettings($r, $settings->getParams());
			else:
				$this->setSettings($r, $settings);
			endif;
		endif;
		
		// Valid the Router type
		if($contract !== RouterTypeInterface::class && ! $r instanceof RouterTypeInterface):
			$msg = sprintf("Handler %s is not a instance of %s.", $handler, $contract);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		$this->setRouterPlugin($r);
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $settings
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _resolveRouterType(array $settings) : void {
		if(( $tmp = $settings[AsiRegisterParams::ROUTER_TYPE->value] ?? NULL ) !== NULL):
			// Resolve router type (SIMPLE or TREE)
			if($tmp instanceof RouterType):
				$this->setRouterType($tmp);
			elseif(is_string($tmp) && ( $tf = RouterType::tryFrom($tmp) ) !== NULL):
				$this->setRouterType($tf);
			endif;
		endif;
	}
	
	/**
	 * @param    RouterType    $type
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	private function _getDefaultRouterType(RouterType $type) : ?string {
		return self::DEFAULT_ROUTER_TYPE_LIST[$type->value] ?? NULL;
	}
	
	/**
	 * @return null|RouterType
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouterType() : ?RouterType {
		return $this->_routerType ?? NULL;
	}
	
	/**
	 * @param    RouterType    $type
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouterType(RouterType $type) : static {
		//
		$this->_routerType = $type;
		
		//
		return $this;
	}
	
	/**
	 * @param    RouterTypeInterface    $plugin
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouterPlugin(RouterTypeInterface $plugin) : static {
		//
		$this->_routerTypePlugin = $plugin;
		
		//
		return $this;
	}
	
	/**
	 * @return null|RouterTypeInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouterPlugin() : ?RouterTypeInterface {
		return $this->_routerTypePlugin ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouterType() : static {
		//
		unset($this->_routerType);
		
		//
		return $this;
	}
	
	/**
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouterModel() : RouterInterface {
		//
		$this->_routerModel = self::DEFAULT_ROUTER_MODEL;
		
		//
		return $this;
	}
	
	/**
	 * @param    RouteParams     $name
	 * @param    MapInterface    $map
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function addRouteMap(RouteParams $name, MapInterface $map) : void {
		$this->_routeMap[$name->value] = $map;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteMaps() : array {
		return $this->_routeMap ?? [];
	}
	
	/**
	 * @param    RouteParams    $name
	 *
	 * @return null|MapInterface|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteMap(RouteParams $name) : MapInterface|array|null {
		return $this->_routeMap[$name->value] ?? NULL;
	}
	
	/**
	 * @param    null|RouterModel    $route
	 *
	 * @return array|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRoute(?RouterModel $route = NULL) : array|string {
		return $this->_route;
	}
	
	/**
	 * @param    string    $route
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRoute(string $route) : RouterInterface {
		//
		$this->_route = $route;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $route
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRoute(string $route) : RouterInterface {
		//
		unset($this->_route);
		
		//
		return $this;
	}
	
	/**
	 * @param    RequestInterface    $request
	 *
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequest(RequestInterface $request) : RouterInterface {
		//
		$this->_requestInterface = $request;
		
		//
		return $this;
	}
	
	/**
	 * @return null|RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequest() : ?RequestInterface {
		return $this->_requestInterface ?? NULL;
	}
	
	/**
	 * @return RouterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRequest() : RouterInterface {
		//
		unset($this->_requestInterface);
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getLanguageSettings() : array {
		return $this->_languageSettings;
	}
	
	/**
	 * @param    array    $settings
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setLanguageSettings(array $settings) : static {
		if(! empty($settings)):
			$accept = array_keys(self::DEFAULT_LANGUAGE_SETTINGS);
			foreach($settings as $key => $val):
				$key = strtolower($key);
				if(in_array($key, $accept, TRUE) === TRUE):
					$this->_languageSettings[$key] = strtolower($val);
				else:
					$msg = sprintf("Given key %s is not supported in language settings. Accepted values are: %s.", $key,
					               implode(', ', $accept));
					throw new InvalidArgumentException($msg);
				endif;
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getLanguageSetting(string $key) : mixed {
		return $this->_languageSettings[$key] ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetLanguageSettings() : static {
		//
		$this->_languageSettings = self::DEFAULT_LANGUAGE_SETTINGS;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getLanguage() : ?array {
		return $this->getParam(AsiRegisterParams::LANGUAGE->value);
	}
	
	/**
	 * @param    string|array    $lang
	 * @param    string          $default
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setLanguage(string|array $lang, string $default = 'en') : static {
		//
		if(is_string($lang)):
			$lang = (array)$lang;
		endif;
		
		//
		if(! empty($lang)):
			//
			$result = [];
			$valid  = $this->_validLanguage();
			
			// 
			foreach($lang as $val):
				$value = mb_strtolower(trim($val), $this->getEncoding());
				
				// Accept lang value
				if(in_array($value, $valid, TRUE)):
					$result[] = $value;
				endif;
			endforeach;
			
			// If given lang is not same as default then add default all to end of array
			$def = mb_strtolower(trim($default), $this->getEncoding());
			if(in_array($def, $result, TRUE) === FALSE):
				$result[] = $def;
			endif;
			
			// Set language param
			if(! empty($result)):
				$this->setParam(AsiRegisterParams::LANGUAGE->value, $result);
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Return valid language codes (ISO 639-1)
	 *
	 * @return string[]
	 * @since   3.0.0 First time introduced.
	 */
	final protected function _validLanguage() : array {
		return ['aa', 'ab', 'ae', 'af', 'ak', 'am', 'an', 'ar', 'as', 'av', 'ay', 'az', 'ba', 'be', 'bg', 'bi', 'bm',
		        'bn', 'bo', 'br', 'bs', 'ca', 'ce', 'ch', 'co', 'cr', 'cs', 'cu', 'cv', 'cy', 'da', 'de', 'dv', 'dz',
		        'ee', 'el', 'en', 'eo', 'es', 'et', 'eu', 'fa', 'ff', 'fi', 'fj', 'fo', 'fr', 'fy', 'ga', 'gd', 'gl',
		        'gn', 'gu', 'gv', 'ha', 'he', 'hi', 'ho', 'hr', 'ht', 'hu', 'hy', 'hz', 'ia', 'id', 'ie', 'ig', 'ii',
		        'ik', 'io', 'is', 'it', 'iu', 'ja', 'jv', 'ka', 'kg', 'ki', 'kj', 'kk', 'kl', 'km', 'kn', 'ko', 'kr',
		        'ks', 'ku', 'kv', 'kw', 'ky', 'la', 'lb', 'lg', 'li', 'ln', 'lo', 'lt', 'lu', 'lv', 'mg', 'mh', 'mi',
		        'mk', 'ml', 'mn', 'mr', 'ms', 'mt', 'my', 'na', 'nb', 'nd', 'ne', 'ng', 'nl', 'nn', 'no', 'nr', 'nv',
		        'ny', 'oc', 'oj', 'om', 'or', 'os', 'pa', 'pi', 'pl', 'ps', 'pt', 'qu', 'rm', 'rn', 'ro', 'ru', 'rw',
		        'sa', 'sc', 'sd', 'se', 'sg', 'si', 'sk', 'sl', 'sm', 'sn', 'so', 'sq', 'sr', 'ss', 'st', 'su', 'sv',
		        'sw', 'ta', 'te', 'tg', 'th', 'ti', 'tk', 'tl', 'tn', 'to', 'tr', 'ts', 'tt', 'tw', 'ty', 'ug', 'uk',
		        'ur', 'uz', 've', 'vi', 'vo', 'wa', 'wo', 'xh', 'yi', 'yo', 'za', 'zh', 'zu',];
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetLanguage() : static {
		//
		unset($this->_language);
		
		//
		return $this;
	}
}
